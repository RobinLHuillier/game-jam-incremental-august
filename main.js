/*
------------------------------
PREVIOUS VERSIONS
------------------------------

XXXX 0.1 PROTOTYPE:
*** supermarché affiché avec travailleurs qui refournissent les rayons et clients qui achètent des trucs ***
X-> obj Product : affichage + nom + quantité
X-> productHandler : liste produits + interroger stocks
X-> obj Shelf : liste contenu + position d'affichage + position d'attrapage + retirer/augmenter stock + afficher étagère + afficher contenu
X-> marketHandler : liste des étagères + collisions + donner points de destination + interroger stocks + drawbackground
X-> obj Customer : produit préféré + affichage + destination
X-> customerHandler : liste clients + déplacement/collisions + changer destination + affichage + générer clients
X-> path algorithm
X-> class People extended by customer and employee
X-> obj Employee : destination (stock->shelf) en fonction de ce qui est le moins en rayon + affichage
X-> employeeHandler : liste employés + déplacement/collisions + changer destination + affichage
X-> ajouter des images pour chaque assets (simples pour le test)
    X-> employee, customer, shelf, wall, register, products
X-> gameHandler.core : 
    X-> init affichage supermarché + rayons + contenu rayons
    X-> loop employees and customers + affichage
X-> ajouter les saves correctement

XXXX 0.2 INFORMATIONS:
*** montrer plus d'infos sur ce qui est en train de se passer dans le magasin ***
X-> Liste sur la gauche des produits en stock (arrière-boutique)
    X-> une liste mise à jour de ce qui est visible réaffichée que sur de nouvelles infos
    X-> une liste des quantités sur un canvas réaffiché constamment (front)
    X-> correctitude du prix affichée
X-> Bouton empêcher la vente (modifier produit + save + clic effect)
X-> Bulles infos au dessus de la tête des customers
    X-> effet content/mécontent/produit visé/register/sortie
    X-> Afficher + timer de désaffichage
    
XXXX 0.3 SECURING DEALS:
X-> afficher tickets gagnés (ticketHandler?)
X-> pouvoir monter ou diminuer le prix des produits
    X-> hover sur les prod pour voir le nom
X-> onglets deals (dealHandler?) affiché
X-> objet deal à générer et ajouter à la liste
X-> deals arrivent régulièrement (blackMarket), proposent une certaine quantité de produits pour un prix, ont un temps limite d'achat (barre qui diminue)
X-> cliquer sur un deal pour l'acheter (si assez de thune)
X-> certains deals viennent du gouvernement, sont OBLIGATOIRES (barre plus longue, une fois au bout achat automatique)
X-> liste de données qui donne le moment et le temps des deals gouvernementaux, se calcule automatiquement
X-> les deals gouvernementaux débloquent des ressources primaires
X-> save enregistre les deals actuels + tickets
X-> si les deals gouvernementaux ne sont pas payables (pas assez d'argent), on est envoyés au goulag (lose condition)

XXXX 0.4 DEVELOPING NEW IDEAS
X-> arbre d'idées dessiné
X-> onglet ideas à afficher (ideaHandler)
X-> objet idea (Upgrade)
X-> tooltips idées qui donnent un peu plus d'infos
X-> les idées ont un temps de recherche avec affichage d'une barre de progression
X-> idées débloquent d'autres idées, ou des types d'employés, ou autre, servent à montrer l'histoire
X-> implémenter toutes les idées qui ne débloquent pas de travail
X-> save compliant

XXXX 0.5 BACK WORK
X-> objet WorkStation
X-> onglet work (workHandler)
X-> affiche les types de travaux disponibles (x6)
X-> affiche le nombre d'employés en magasin (pouvoir augmenter/diminuer ce nombre)
X-> pouvoir y assigner des travailleurs
X-> calcul automatique de la montée/descente des ressources en fonction
X-> ajouter les produits secondaires à la liste des produits de base

XXXX 0.6 MECHANICALLY SUFFICIENT
X-> le fait que ce soit très cher pousse l'acheteur à voler (goal spécial)
X-> cliquer dessus permet de le mettre au gulag
X-> peut se faire automatiquement à la sortie une fois que l'option est débloquée

XXXX 0.7 VISUAL FLUFFING
X-> options (crédits + erase save)
X-> import/export
X-> afficher save quand elle a lieu
X-> version 32x32 pour les ressources pour un affichage plus net
X-> entourer la tronche des smiley de noir pour qu'on les voit mieux
X-> employé ne clignote pas en attendant d'avoir du stock à déposer (invisible tant que pas de stock?)
X-> mettre en place toutes les shelves
X-> ajouter des visuels à chaque produits
X-> tiles au sol du magasin
X-> help
X-> si pas de musique, retirer les crédit d'Anttis
X-> chargement permet de charger font
X-> murs, portes sortie et arrière du magasin
X-> Menu de lose
X-> Menu de victoire
X-> upgrade pour accepter automatiquement les deals à 1 pour 1
X-> ajouter upgrades pour doubler le nombre de customers en magasin
X-> upgrade pour réattirer gens moins pauvres
X-> times of ideas
X-> correctPrice
X-> govDeals time
X-> govDeal loop

0.71 HOTFIX
X-> ungulaged count didn't increase when decreasing restockers
X-> blackmarket deals no longer shuffling around when one is finished
X-> time incorrectly passing for ideas, making researching 1.5x longer than intended

*/

/*
------------------------------
GOALS
------------------------------

0.8 RUNNING OUT OF TIME

-> Musique et sons (fin de recherche, approche de deal gov)
-> couper musique
-> changer la tronche des employés/customers
-> employé dépose ce qu'il transporte un par un
-> changer le visuel des boutons clickables
-> si un onglet est ouvert, la couleur du nom de l'onglet est différente

0.X FOURRE-TOUT:

0.X BUGS:

/*
------------------------------
GLOBAL VARIABLES
------------------------------
*/ 

//canvas stats
var canvas = {width: 1300, height: 700};
//canvas layers
var canvasBackground = document.getElementById('canvasBackground');
var ctxBG = canvasBackground.getContext('2d');
var canvasShelf = document.getElementById('canvasShelf');
var ctxShelf = canvasShelf.getContext('2d');
var canvasPeople = document.getElementById('canvasPeople');
var ctxPeople = canvasPeople.getContext('2d');
var canvasUI = document.getElementById('canvasUI');
var ctxUI = canvasUI.getContext('2d');
var canvasInfos = document.getElementById('canvasInfos');
var ctxInfos = canvasInfos.getContext('2d');
var canvasFront = document.getElementById('canvasFront');
var ctxFront = canvasFront.getContext('2d');

//mouse
var canvasPosition = canvasFront.getBoundingClientRect();
const mouse = {
    x: undefined,
    y: undefined,
};
//time
const refresh = 20; //refresh rate in ms
var lastTime = 0;
var timer;
var deltaTimeElapsed = 0;
//img, sound, font
var img = {"customer": new Image(), "employee": new Image(), "register": new Image(), "shelf": new Image(), "wheat_flour": new Image(), "cow_milk": new Image(), "rapeseed_oil": new Image(), "potato": new Image(), "happy": new Image(), "unhappy": new Image(), "very_happy": new Image(), "very_unhappy": new Image(), "no_emotion": new Image(), "exit": new Image(), "up": new Image(), "down": new Image(), "option": new Image(), "save": new Image(), "musicOn": new Image(), "musicOff": new Image(), "help": new Image(), "cow_milk32": new Image(), "potato32": new Image(), "wheat_flour32": new Image(), "rapeseed_oil32": new Image(), "rob": new Image(), "buckwheat_flour": new Image(), "buckwheat_flour32": new Image(), "seasonal_vegetables": new Image(), "seasonal_vegetables32": new Image(), "potluck_soup": new Image(), "potluck_soup32": new Image(), "soap": new Image(), "soap32": new Image(), "skim_milk": new Image(), "skim_milk32": new Image(), "vodka": new Image(), "vodka32": new Image(), "frying_oil": new Image(), "frying_oil32": new Image(), "beef_meat": new Image(), "beef_meat32": new Image(), "ground_beef": new Image(), "ground_beef32": new Image(), "great_leader": new Image(), "great_leader32": new Image(), "collectible_thumbnails": new Image(), "collectible_thumbnails32": new Image(), "tile": new Image(), "stock_door": new Image(), "front_door": new Image()};
var sound = {};
//import/export
var importExport = document.getElementById("importExport");
var infoImportExport = document.getElementById("infoImportExport");
var contentImportExport = document.getElementById("contentImportExport");
//lol
const tcross = "Ŧ";

/*
------------------------------
CLASSES
------------------------------
*/

/**
 * Idea Class
 * ---------------------
 * sliceMyTooltip() -> Array of strings (slice tooltips in lines)  
 * draw(x,y)  
 * drawTooltip(x,y)  
 * advanceTime() -> bool if finished  
 */
class Idea {
    /**
     * @param {string} name 
     * @param {number} time 
     * @param {string} text1
     * @param {string} text2 
     * @param {string} tooltip 
     * @param {Array} unlock array of string 
     * @param {string} effect 
     */
    constructor(name, time, text1, text2, tooltip, unlock, effect, visible = false) {
        this.name = name;
        this.startTime = time;
        this.remainingTime = time;
        this.visible = visible;
        this.text1 = text1;
        this.text2 = text2;
        this.tooltip = tooltip;
        this.tooltip = this.sliceMyTooltip();
        this.unlock = unlock;
        this.effect = effect;
        this.searching = false;
    }
    /**
     * cut the tooltip in array of strings
     * @returns {Array} of strings
     */
    sliceMyTooltip() {
        let toolSlice = new Array();
        const maxLength = 30;
        const offset = 10;
        let min = 0;
        let max = min + maxLength-offset;
        while(max < this.tooltip.length) {
            if(this.tooltip[max] === " ") {
                toolSlice.push(this.tooltip.slice(min, max));
                min = max + 1;
                max = min + maxLength - offset;
            } else {
                max++;
            }
        }
        toolSlice.push(this.tooltip.slice(min));
        return toolSlice;
    }
    /**
     * draw on front
     * @param {number} x 
     * @param {number} y 
     */
    draw(x,y) {
        const h = 110;
        const w = 270;
        if(this.searching) {
            ctxFront.fillStyle = "rgba(240, 52, 52, 1)";
        } else {
            ctxFront.fillStyle = "rgba(245, 215, 110, 1)";
        }        
        ctxFront.strokeStyle = "black";
        ctxFront.lineWidth = 3;
        ctxFront.fillRect(x,y,w,h);
        ctxFront.strokeRect(x,y,w,h);
        ctxFront.fillStyle = "black";
        ctxFront.font = "25px kenney_miniregular";
        ctxFront.fillText(this.text1, x+5, y+25, w-10);
        ctxFront.fillText(this.text2, x+5, y+50, w-10);
        ctxFront.fillText(ticksToText(Math.round(this.remainingTime)), x+20, y+80, 200);
        ctxFront.fillStyle = "rgba(30, 130, 76, 1)";
        let ratioTime = this.remainingTime/this.startTime;
        ctxFront.fillRect(x+5,y+h-15, (w-10)*ratioTime, 10);
    }
    /**
     * @param {number} x 
     * @param {number} y 
     */
    drawTooltip(x,y) {
        const w = 400;
        const h = 55;
        const off = 10;
        ctxFront.lineWidth = 3;
        ctxFront.strokeStyle = "black";
        ctxFront.fillStyle = "rgba(228, 233, 237, 1)";
        ctxFront.beginPath();
        ctxFront.moveTo(x,y);
        ctxFront.lineTo(x-off,y-off);
        ctxFront.lineTo(x-off,y-h);
        ctxFront.lineTo(x-w,y-h);
        ctxFront.lineTo(x-w,y+h);
        ctxFront.lineTo(x-off,y+h);
        ctxFront.lineTo(x-off,y+off);
        ctxFront.lineTo(x,y);
        ctxFront.stroke();
        ctxFront.fill();
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        for(let i=0; i<this.tooltip.length; i++) {
            ctxFront.fillText(this.tooltip[i], x-w+10, y-25+30*i, w-30);
        }
    }
    /**
     * @returns {boolean} true if no time left
     */
    advanceTime() {
        this.remainingTime -= deltaTimeElapsed/refresh;
        if(this.remainingTime <= 0)
            return true;
        return false;
    }
}

/**
 * Generic button class
 * --------------------
 * clic -> string if clic inside / undefined if clic outside 
 * draw()
 * rgba(color) -> str rgba code for "red"/etc.
 * display()
 * hide()
 * addText(text, fontSize, fontColor, x, y, maxWidth)
 */
class Button {
    /**
     * @param {number} x left
     * @param {number} y top
     * @param {number} w width
     * @param {number} h height
     * @param {boolean} clickable if true, show little corners 
     * @param {string} color background color (default yellow)
     * @param {Object} ctxParam layer onto we draw (default to front)
     * @param {boolean} visible
     * @param {string} effect name of the effect provocked by a clic
     * @param {string} name name of the menu
     */
    constructor(x,y,w,h,clickable,color,ctxParam,visible,effect = undefined, name = "") {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.clickable = clickable;
        this.color = this.colorToRgba(color);
        this.ctx = ctxParam;
        this.visible = visible;
        this.effect = effect;
        this.textList = new Array();
        this.name = name;
    }
    /**
     * @param {number} x position x of the mouse
     * @param {number} y position y of the mouse
     * @returns {string | undefined} if clic reached, else undefined
     */
    clic(x,y) {
        if(this.visible && this.clickable && x >= this.x && x <= this.x+this.w && y > this.y && y < this.y+this.h)
            return this.effect;
        else
            return undefined;
    }
    /**
     * @param {string} color 
     * @returns {string} color coded to rgba
     */
    colorToRgba(color) {
        let rgba;
        switch(color) {
            case "red":
                rgba = "rgba(240, 52, 52, 1)";
                break;
            case "green":
                rgba = "rgba(30, 130, 76, 1)";
                break;
            case "grey":
                rgba = "rgba(109, 113, 122, 1)";
                break;
            case "lightgrey":
                rgba = "rgba(228, 233, 237, 1)";
                break;
            case "brown":
                rgba = "rgba(164, 116, 73, 1)";
                break;
            case "yellow2":
                rgba = "rgba(245, 215, 110, 1)";
                break;
            case "invisible":
                rgba = "invisible";
                break;
            case "black":
                rgba = "rgba(0,0,0,1)";
                break;
            case "yellow":
            default:
                rgba = "rgba(247, 202, 24, 1)";
                break;
        }
        return rgba;
    }
    draw() {
        this.ctx.clearRect(this.x, this.y, this.w, this.h);
        if(this.visible) {
            //contour
            this.ctx.lineWidth = 3;
            this.ctx.fillStyle = "rgba(150, 40, 27, 1)";
            this.ctx.strokeStyle = "black";
            this.ctx.fillRect(this.x, this.y, this.w, this.h);
            this.ctx.strokeRect(this.x, this.y, this.w, this.h); 
            if(this.color !== "invisible") {
                this.ctx.fillStyle = this.color;
                this.ctx.fillRect(this.x+5, this.y+5,this.w-10,this.h-10);
            } else {
                this.ctx.clearRect(this.x+5, this.y+5,this.w-10,this.h-10);
            }
            this.ctx.strokeRect(this.x+5, this.y+5,this.w-10,this.h-10);
            if(this.clickable) {
                this.ctx.fillStyle = "rgba(108, 122, 137, 1)";
                this.ctx.lineWidth = 5;
                this.ctx.beginPath();
                this.ctx.arc(this.x+2.5,this.y+2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+this.w-2.5,this.y+this.h-2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+2.5,this.y+this.h-2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+this.w-2.5,this.y+2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
            }
            //text
            for(let text of this.textList) {
                this.ctx.fillStyle = text.fontColor;
                this.ctx.font = text.fontSize;
                this.ctx.fillText(text.text,text.x,text.y,text.maxWidth);
            }
        }
    }
    display() {
        this.visible = true;
    }
    hide() {
        this.visible = false;
    }
    /**
     * @param {string} text 
     * @param {string} fontSize "30px Verdana" 
     * @param {string} fontColor "red"
     * @param {number} x 
     * @param {number} y 
     * @param {number} maxWidth 
     */
    addText(text, fontSize, fontColor, x, y, maxWidth) {
        let rgba = this.colorToRgba(fontColor);
        this.textList.push({"text": text, "fontSize": fontSize, "fontColor": rgba, "x": x, "y": y, "maxWidth": maxWidth}); 
    }
}

/**
 * Product Class
 * -------------
 * add(num) -> void  
 * subtract(num) -> void  
 * getQuantity() -> num  
 * getPrice() -> num  
 * augmentPrice(augmentation) -> void  
 * decreasePrice(decrease) -> void  
 * getImg() -> Image  
 * getName() -> str  
 * becomeVisible()  
 * stopSales()  
 * beginSales()  
 */
class Product {
    /**
     * @param {string} name 
     * @param {Image} img 
     * @param {number} quantity 
     * @param {number} correctPrice
     */
    constructor(name, img, quantity, correctPrice, secondary = false) {
        this.name = name;
        this.img = img;
        this.quantity = quantity;
        this.visible = false;
        this.price = correctPrice;
        this.correctPrice = correctPrice;
        this.sellIt = true;
        this.secondary = secondary;
    }
    /**
     * @param {number} quantity default 1
     */
    add(quantity = 1) {
        this.quantity += quantity;
    }
    /**
     * @param {number} quantity default 1
     */
    subtract(quantity = 1) {
        console.assert(this.quantity >= quantity, this.quantity, quantity);
        if(this.quantity >= quantity) {
            this.quantity -= quantity;
        }
    }
    /**
     * @returns {number} quantity
     */
    getQuantity() {
        return this.quantity;
    }
    /**
     * @returns {number} -2 extremely unhappy/-1 unhappy/0 correct/1 happy/2 extremely happy
     */
    getEmotionDueToPrice() {
        return this.correctPrice - this.price;
        /*
        let deltaPrice = this.correctPrice - this.price;
        if(deltaPrice <= -2) {
            return -2;
        } else if(deltaPrice >= 2) {
            return 2;
        }
        return deltaPrice;*/
    }
    /**
     * @returns {number} price
     */
    getPrice() {
        return this.price;
    }
    /**
     * @param {number} augmentation 
     */
    augmentPrice(augmentation = 1) {
        this.price += augmentation;
    }
    /**
     * @param {number} decrease 
     */
    decreasePrice(decrease = 1) {
        this.price -= decrease;
        if(this.price < 1)
            this.price = 1;
    }
    /**
     * @returns {Image}
     */
    getImg() {
        return this.img;
    }
    /**
     * @returns {string} name
     */
    getName() {
        return this.name;
    }
    becomeVisible() {
        this.visible = true;
    }
    beginSales() {
        this.sellIt = true;
    }
    stopSales() {
        this.sellIt = false;
    }
}

/**
 * Shelf Class
 * -----------
 * addProduct(name, quantity)  
 * subtract(num)  
 * getProduct() -> {name, quantity}  
 * getAccessPoint() -> {x, y}  
 * getQuantityAddable() -> num  
 * drawShelf()  
 * drawShelfContent()  
 * doICollide(x,y,w,h) -> bool
 */
class Shelf {
    /**
     * @param {number} x 
     * @param {number} y 
     * @param {string} frontPosRetrieving ["left", "right", "top", "bottom"]
     */
    constructor(x, y, frontPosRetrieving) {
        this.x = x;
        this.y = y;
        this.w = 64;
        this.h = 64;
        this.img = img.shelf;
        this.productStocked = "none";
        this.quantityStocked = 0;
        this.maxStocked = 16;
        //access point
        this.destX = this.x;
        this.destY = this.y;
        this.frontPosRetrieving = frontPosRetrieving;
        switch(frontPosRetrieving) {
            case "left":
                this.destX -= 38;
                break;
            case "right":
                this.destX += 70;
                break;
            case "top":
                this.destY -= 70;
                break;
            case "bottom":
                this.destY += 70;
                break;
        }
    }
    /**
     * @param {string} name default to name of product already stocked
     * @param {number} quantity 
     */
    addProduct(name, quantity) {
        console.assert(this.productStocked === "none" || this.productStocked === name, name, this.productStocked);
        console.assert(this.quantityStocked + quantity <= this.maxStocked, this.quantityStocked, quantity, this.maxStocked);
        this.productStocked = name;
        this.quantityStocked += quantity;
        this.drawShelfContent();
    }
    /**
     * @param {number} quantity 
     */
    subtract(quantity) {
        console.assert(this.quantityStocked >= quantity, quantity, this.quantityStocked);
        this.quantityStocked -= quantity;
        if(this.quantityStocked <= 0) {
            this.quantityStocked = 0;
            this.productStocked = "none";
        }
        this.drawShelfContent();
    }
    /**
     * @returns {Object} {name: str, quantity: num}
     */
    getProduct() {
        return {name: this.productStocked, quantity: this.quantityStocked};
    }
    /**
     * @returns {Object} {x: num, y: num} center of access point
     */
    getAccessPoint() {
        return {x: this.destX, y: this.destY};
    }
    /**
     * @returns {number} quantity
     */
    getQuantityAddable() {
        return this.maxStocked - this.quantityStocked;
    }
    /**
     * draw on ctxBG
     */
    drawShelf() {
        ctxBG.drawImage(this.img, this.x, this.y, this.w, this.h);
    }
    /**
     * clear size of shelf  
     * draw on ctxShelf
     */
    drawShelfContent() {
        ctxShelf.clearRect(this.x, this.y, this.w, this.h);
        if(this.productStocked !== "none") {
            let img = productHandler.getProductImg(this.productStocked);
            const w = 15;
            const h = 15;
            const offset = 2;
            let x = 0;
            let y = 0;
            let drawn = 0;
            while(drawn < this.quantityStocked) {
                ctxShelf.drawImage(img, this.x + offset + w*x, this.y + offset + h*y, w, h);
                x++;
                if(x*w + offset >= this.w - offset) {
                    x = 0;
                    y++;
                }
                drawn++;
            }
        }
    }
    /**
     * @param {number} x 
     * @param {number} y 
     * @param {number} w width
     * @param {number} h height
     * @returns {boolean} collision or not
     */
    doICollide(x,y,w,h) {
        return (x <= this.x + this.w && x + w > this.x && y <= this.y + this.h && y + h > this.y);
    }
}

/**
 * People class, generic
 * ---------------------
 * decay()  
 * getPos() -> {x, y}  
 * getSize() -> {w, h}  
 * getSpeed() -> num  
 * getGoal() -> {x, y}  
 * move()  
 * draw()  
 * setGoal(goal, goalName) {dest: {x,y}, shelf: Shelf}  
 * getShelf() -> Shelf  
 * getGoal() -> {x,y}
 * reachGoal()  
 */
class People {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.w = 32;
        this.h = 64;
        this.visible = false;

        this.img = null;
        
        this.direction = "top";
        const dir = ["top", "bottom"];
        this.preferredDirection = dir[Math.floor(Math.random()*2)];
        this.speed = 4;

        this.goal = undefined;
        this.goalName = undefined;

        this.currentAnim = 0;
        this.delayAnim = 0;
        this.maxDelayAnim = 5;
        this.maxAnim = 6;
    }
    /**
     * hide the people
     */
    decay() {
        this.visible = false;
    }
    /**
     * @returns {Object} {x, y}
     */
    getPos() {
        return {x: this.x, y: this.y};
    }
    /**
     * @returns {Object} {w, h}
     */
    getSize() {
        return {w: this.w, h: this.h};
    }
    /**
     * @returns {number}
     */
    getSpeed() {
        return this.speed;
    }
    /**
     * @returns {string} preferredDirection ("top", etc.)
     */
    getPreferredDirection() {
        return this.preferredDirection;
    }
    /**
     * @param {string} dir 
     */
    move(dir) {
        switch(dir) {
            case "top":
                this.y -= this.speed;
                break;
            case "left":
                this.x -= this.speed;
                break;
            case "right":
                this.x += this.speed;
                break;
            case "bottom":
                this.y += this.speed;
                break;
        }
        this.direction = dir;
    } 
    /**
     * @param {Object} goal {dest: {x,y}, shelf: Shelf} 
     * @param {string} goalName
     */
    setGoal(goal, goalName) {
        this.goal = {"dest": {x: goal.dest.x, y: goal.dest.y}, "shelf": goal.shelf};
        this.goalName = goalName;
    }
    /**
     * @returns {Shelf | undefined} shelf
     */
    getShelf() {
        return this.goal?.shelf;
    }
    /**
     * @returns {Object} {x, y}
     */
    getGoal() {
        return this.goal?.dest;
    }
    reachGoal() {
        this.goal = undefined;
        this.goalName = undefined;
    }
    /**
     * draw on ctxPeople
     */
    draw() {
        const imgWidth = 32;
        const imgHeight = 64;
        let offset = 0;
        switch(this.direction) {
            case "right":
                break;
            case "top":
                offset = 6;
                break;
            case "left":
                offset = 12;
                break;
            case "bottom":
                offset = 18;
                break;
        }
        ctxPeople.drawImage(this.img, imgWidth*(offset+this.currentAnim), 0, imgWidth, imgHeight, this.x, this.y, this.w, this.h);
        this.delayAnim++;
        if(this.delayAnim >= this.maxDelayAnim) {
            this.delayAnim = 0;
            this.currentAnim++;
            if(this.currentAnim >= this.maxAnim) {
                this.currentAnim = 0;
            }
        }
    }
}

/**
 * Customer Class
 * --------------
 * init(x,y,preferredProduct,maxTickets)  
 * decay()  
 * addToCart(productName, price)  
 * getTickets() -> num   
 * getCartList() -> Array  
 * getPreferredDirection() -> str  
 * addBubble(type)  
 * pay()  
 */
class Customer extends People {
    constructor() {
        super();
        this.img = img.customer;

        this.preferredProduct = "none";
        this.transporting = new Array();
        this.maxTickets = 0;
        this.currentPriceToPay = 0;
        this.currentMood = 0;

        this.displayBubble = false;
        this.infoBubble = "none";
        this.timingBubble = 0;
    }
    /**
     * reinit the customer to prepare use
     * @param {number} x 
     * @param {number} y 
     * @param {string} preferredProduct 
     * @param {number} maxTickets 
     */
    init(x,y,preferredProduct,maxTickets) {
        this.visible = true;
        this.x = x;
        this.y = y;
        this.preferredProduct = preferredProduct;
        this.maxTickets = maxTickets;
        this.currentPriceToPay = 0;
        this.goal = undefined;
        this.goalName = undefined;
        this.direction = "top";
        this.currentAnim = 0;
        this.delayAnim = 0;
        this.transporting = new Array();

        this.displayBubble = false;
        this.infoBubble = "none";
        this.timingBubble = 0;

        this.currentMood = 0;
    }
    /**
     * @param {string} productName 
     * @param {number} price 
     */
    addToCart(productName, price) {
        console.assert(price <= this.maxTickets - this.currentPriceToPay, price, this.maxTickets, this.currentPriceToPay);
        this.transporting.push(productName);
        this.currentPriceToPay += price;
    }
    /**
     * @returns {number} tickets
     */
    getTickets() {
        return this.maxTickets - this.currentPriceToPay;
    }
    /**
     * @returns {Array}
     */
    getCartList() {
        return this.transporting;
    }
    /**
     * @param {string} productName 
     */
    addTransport(productName) {
        this.transporting.push(productName);
    }
    /**
     * @param {string} type happy/unhappy/product/register/exit
     */
    addBubble(type) {
        this.timingBubble = 20;
        this.infoBubble = type;
        this.displayBubble = true;
    }
    pay() {
        //chances to rob
        if(this.currentMood < 0) {
            let chances = Math.abs(this.currentMood)/20;
            if(Math.random() < chances) {
                this.addBubble("rob");
                this.timingBubble = 15000;
            } else {
                ticketHandler.add(this.currentPriceToPay);
            }
        } else {
            ticketHandler.add(this.currentPriceToPay);
        }
    }
    draw() {
        super.draw();
        if(this.displayBubble) {
            //draw Bubble
            const w = 15;
            const h = 30;
            const off = 3;
            let x = this.x+this.w/2;
            let y = this.y+20;
            ctxPeople.strokeStyle = "black";
            ctxPeople.fillStyle = "white";
            ctxPeople.lineWidth = 3;
            ctxPeople.beginPath();
            ctxPeople.moveTo(x,y);
            ctxPeople.lineTo(x-off,y-off);
            ctxPeople.lineTo(x-w, y-off);
            ctxPeople.lineTo(x-w, y-off-h);
            ctxPeople.lineTo(x+w, y-off-h);
            ctxPeople.lineTo(x+w, y-off);
            ctxPeople.lineTo(x+off, y-off);
            ctxPeople.lineTo(x,y);
            ctxPeople.stroke();
            ctxPeople.fill();

            //draw infoBubble
            let image = undefined;
            switch(this.infoBubble) {
                case "happy":
                    image = img.happy;
                    break;
                case "unhappy":
                    image = img.unhappy;
                    break;
                case "register":
                    image = img.register;
                    break;
                case "exit":
                    image = img.exit;
                    break;
                case "rob":
                    image = img.rob;
                    break;
            }
            if(image === undefined) {
                //it's a product
                image = productHandler.getProductImg(this.infoBubble);
            }
            ctxPeople.drawImage(image, x-w+5, y-off-h+5, 20,20);

            this.timingBubble--;
            if(this.timingBubble <= 0) {
                this.timingBubble = 0;
                this.infoBubble = "none";
                this.displayBubble = false;
            }
        }
    }
}

/**
 * Employee Class
 * --------------
 * init(x,y)  
 * addTransporting(productName, quantity)  
 * subtractTransporting(quantity)  
 * getRemainingTransportCapacity() -> num  
 * reinitTransport()  
 * getTransport()  
 */
class Employee extends People {
    constructor() {
        super();
        this.img = img.employee;

        this.transporting = {};
        this.transporting.productName = undefined;
        /** @type {undefined | number} */
        this.transporting.quantity = undefined;
        this.maxTransporting = 8;

        this.preferredDirection = "top";
    }
    /**
     * @param {number} x 
     * @param {number} y 
     */
    init(x, y) {
        this.visible = true;
        this.x = x;
        this.y = y;
        this.goal = undefined;
        this.goalName = undefined;
        this.direction = "top";
        this.currentAnim = 0;
        this.delayAnim = 0;
        this.transporting = {"productName": undefined, "quantity": undefined};
        this.maxTransporting = 8;
    }
    /**
     * @param {string} productName 
     * @param {number} quantity 
     */
    addTransporting(productName, quantity) {
        if(this.transporting.productName === undefined) {
            this.transporting.productName = productName;
            this.transporting.quantity = quantity;
        } else {
            this.transporting.quantity += quantity;
        }
    }
    /**
     * reinit transporting if quantity decrease to 0
     * @param {number} quantity 
     */
    subtractTransporting(quantity) {
        if(!isNaN(this.transporting.quantity)) {
            this.transporting.quantity -= quantity;
            if(this.transporting.quantity <= 0)
                this.reinitTransport();
        }
    }
    /**
     * @returns {number} capacity
     */
    getRemainingTransportCapacity() {
        if(this.transporting.quantity !== undefined)
            return this.maxTransporting - this.transporting.quantity;
        else 
            return this.maxTransporting;            
    }
    reinitTransport() {
        this.transporting = {"productName": undefined, "quantity": undefined};
    }
    getTransport() {
        return {"productName": this.transporting.productName, "quantity": this.transporting.quantity};
    }
}

/**
 * Deal Class
 * ----------
 * advanceTime() -> bool if finished  
 * draw(x,y)  
 */
class Deal {
    /**
     * @param {string} prodName
     * @param {number} quantity 
     * @param {number} cost 
     * @param {number} time (in number of ticks)
     * @param {boolean} governement or not
     */
    constructor(prodName, quantity, cost, time, governement = false) {
        this.prodName = prodName;
        this.quantity = quantity;
        this.cost = cost;
        this.remainingTime = time;
        this.startTime = time;
        this.governement = governement;
        this.img = productHandler.getProductImg(prodName);
        this.deprecated = false;
    }
    /**
     * @returns {boolean} true if no time left
     */
    advanceTime() {
        this.remainingTime -= deltaTimeElapsed/refresh;
        if(this.remainingTime <= 0)
            return true;
        return false;
    }
    /**
     * draw on front
     * @param {number} x 
     * @param {number} y 
     */
    draw(x,y) {
        let h;
        if(this.governement) {
            h = 110;
            ctxFront.fillStyle = "rgba(245, 215, 140, 1)";
        } else {
            h = 70;
            ctxFront.fillStyle = "rgba(245, 215, 110, 1)";
        }
        const w = 270;
        ctxFront.strokeStyle = "black";
        ctxFront.lineWidth = 3;
        ctxFront.fillRect(x,y,w,h);
        ctxFront.strokeRect(x,y,w,h);
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        ctxFront.drawImage(this.img, x+15, y+7);
        ctxFront.fillText(this.prodName, x+40,y+25, w-45);
        ctxFront.fillText("x"+this.quantity+" for "+this.cost+tcross, x+20, y+50, w-30);
        ctxFront.fillStyle = "rgba(30, 130, 76, 1)";
        let ratioTime = this.remainingTime/this.startTime;
        ctxFront.fillRect(x+5,y+h-15, (w-10)*ratioTime, 10);
    }
}

/**
 * GovDeal Class
 * -------------
 * draw()  
 */
class GovDeal extends Deal{
    /**
     * @param {string} prodName 
     * @param {number} quantity 
     * @param {number} cost 
     * @param {number} time 
     * @param {boolean} visible 
     */
    constructor(prodName, quantity, cost, time, visible = false) {
        super(prodName, quantity, cost, time, true);
        this.visible = visible;
    }
    draw() {
        if(this.visible) {
            super.draw(1015, 120);
            ctxFront.fillStyle = "black";
            ctxFront.fillText(ticksToText(Math.round(this.remainingTime)), 1050, 200, 200);
        }  
    }
}

/**
 * Class WorkStation
 * -----------------
 * sliceMyTooltip() -> Array of strings (slice tooltips in lines)  
 * draw(x,y)  
 * drawTooltip(x,y)  
 * advanceTime() -> bool if finished  
 */
class WorkStation {
    /**
     * @param {string} name 
     * @param {string} productOrigin 
     * @param {string} productGoal 
     * @param {string} tooltip 
     * @param {number} timeToComplete 
     * @param {number} quantityCreated 
     */
    constructor(name, productOrigin, productGoal, tooltip, timeToComplete, quantityCreated) {
        this.name = name;
        this.productOrigin = productOrigin;
        this.productGoal = productGoal;
        this.prodOriginImg = productHandler.getProductImg(productOrigin);
        this.prodGoalImg = productHandler.getProductImg(productGoal);
        this.tooltip = tooltip;
        this.tooltip = this.sliceMyTooltip();
        this.timeToComplete = timeToComplete;
        this.numWorkers = 0;
        this.remainingTime = timeToComplete;
        this.quantityCreated = quantityCreated;
        this.visible = false;
    }
    /**
     * cut the tooltip in array of strings
     * @returns {Array} of strings
     */
    sliceMyTooltip() {
        let toolSlice = new Array();
        const maxLength = 30;
        const offset = 10;
        let min = 0;
        let max = min + maxLength-offset;
        while(max < this.tooltip.length) {
            if(this.tooltip[max] === " ") {
                toolSlice.push(this.tooltip.slice(min, max));
                min = max + 1;
                max = min + maxLength - offset;
            } else {
                max++;
            }
        }
        toolSlice.push(this.tooltip.slice(min));
        return toolSlice;
    }
    /**
     * draw on front
     * @param {number} x 
     * @param {number} y 
     */
    draw(x,y) {
        const h = 70;
        const w = 270;
        ctxFront.fillStyle = "rgba(245, 215, 110, 1)"; 
        ctxFront.strokeStyle = "black";
        ctxFront.lineWidth = 3;
        ctxFront.fillRect(x,y,w,h);
        ctxFront.strokeRect(x,y,w,h);
        ctxFront.fillStyle = "black";
        ctxFront.font = "25px kenney_miniregular";
        ctxFront.fillText(this.name, x+5, y+25, w-80);
        //products images
        ctxFront.drawImage(this.prodOriginImg, x+200, y+8, 16, 16);
        ctxFront.fillText("->" + this.quantityCreated, x+220, y+25, 30);
        ctxFront.drawImage(this.prodGoalImg, x+250, y+8, 16, 16);

        ctxFront.fillText("Employees: " + this.numWorkers, x+5, y+50, w-80);
        //up and down
        ctxFront.drawImage(img.down, x+200, y+34, 16, 16);
        ctxFront.drawImage(img.up, x+220, y+34, 16, 16);

        ctxFront.fillStyle = "rgba(30, 130, 76, 1)";
        let ratioTime = 1-this.remainingTime/this.timeToComplete;
        ctxFront.fillRect(x+5,y+h-15, (w-10)*ratioTime, 10);
    }
    /**
     * @param {number} x 
     * @param {number} y 
     */
    drawTooltip(x,y) {
        const w = 400;
        const h = 55;
        const off = 10;
        ctxFront.lineWidth = 3;
        ctxFront.strokeStyle = "black";
        ctxFront.fillStyle = "rgba(228, 233, 237, 1)";
        ctxFront.beginPath();
        ctxFront.moveTo(x,y);
        ctxFront.lineTo(x-off,y-off);
        ctxFront.lineTo(x-off,y-h);
        ctxFront.lineTo(x-w,y-h);
        ctxFront.lineTo(x-w,y+h);
        ctxFront.lineTo(x-off,y+h);
        ctxFront.lineTo(x-off,y+off);
        ctxFront.lineTo(x,y);
        ctxFront.stroke();
        ctxFront.fill();
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        for(let i=0; i<this.tooltip.length; i++) {
            ctxFront.fillText(this.tooltip[i], x-w+10, y-25+30*i, w-30);
        }
    }
    /**
     * @returns {boolean} true if no time left
     */
    advanceTime() {
        this.remainingTime -= this.numWorkers*deltaTimeElapsed/refresh;
        if(this.remainingTime <= 0) {
            this.remainingTime = this.timeToComplete;
            return true;
        }
        return false;
    }
    addWorker() {
        this.numWorkers++;
    }
    decreaseWorker() {
        this.numWorkers--;
        if(this.numWorkers <= 0)
            this.numWorkers = 0;
    }
}

/*
------------------------------
HANDLERS
------------------------------
*/

var gameHandler = {
    state: "loading",
    /* list all states possible
       loading
       reloading
       playing
       lost
       win
    */
    amIWorking: false, //prevent too much callbacks
    assetsLoaded: 0, //count images loaded
    loading: true,
    fontLoaded: false,
    loseCounter: 0,
    winCounter: 0,

    /** 
     * @nocollapse 
     * */
    initialize() {
        if(this.state == "loading") {       
            //loading assets
            this.initializeImg();
            soundHandler.initialize();
        }

        //other handlers
        UIHandler.initialize();
        upgradeHandler.initialize();
        productHandler.initialize();
        marketHandler.initialize();
        customerHandler.initialize();
        employeeHandler.initialize();
        ticketHandler.initialize();
        dealHandler.initialize();
        ideaHandler.initialize();
        employmentHandler.initialize();

        //launch loop
        if(this.state === "loading")
            animate(performance.now());

        //loading save
        saveHandler.load();
    },
    /**
     * Init all images:  
     * onload call assetsLoading()
     * @nocollapse
     */
    initializeImg() {
        let keys = Object.keys(img);
        for(let key of keys) {
            img[key].onload = function() {gameHandler.assetsLoading()};
            img[key].src = "img/" + key + ".png";
        }
    },
    /** 
     * @nocollapse 
     * */
    assetsLoading() {
        this.assetsLoaded++;
    },
    /**
     * @returns {boolean}
     * @nocollapse
     */
    allAssetsLoaded() {
        return this.assetsLoaded == (Object.keys(img).length + Object.keys(sound).length);
        //add here other types of assets
    },
    /** 
     * @nocollapse 
     * */
    draw() {
        //clear front
        ctxFront.clearRect(0,0,canvas.width, canvas.height);
        ctxPeople.clearRect(0,0,canvas.width, canvas.height);

        //call all draw methods as necessary
        employeeHandler.draw();
        customerHandler.draw();
        UIHandler.drawInfos();
    },
    /** 
     * @nocollapse 
     * */
    core() {
        this.amIWorking = true;
        if(this.state === "loading" || this.state === "reloading") {
            if(this.allAssetsLoaded() && this.fontLoaded) {
                this.state = "playing";
                marketHandler.drawBackground();
                UIHandler.draw();
            } else {
                UIHandler.drawLoading();
                this.fontLoaded = true;
            }
        } else if(this.state === "playing") {
            employeeHandler.core();
            customerHandler.core();
            ideaHandler.core();
            if(dealHandler.govDealUnlocked)
                dealHandler.core();
            if(employmentHandler.employmentUnlocked)
                employmentHandler.core();

            this.draw();
            saveHandler.save();
            upgradeHandler.actualiseVisibility();
        } else if(this.state === "lost") {
            if(this.loseCounter <= 0) {
                this.loseCounter = 500;
            }
            UIHandler.drawLost();
            this.loseCounter--;
            if(this.loseCounter <= 10) {
                this.loseCounter = 0;
                saveHandler.erase();
            }
        } else if(this.state === "win") {
            if(this.winCounter <= 0) {
                this.winCounter = 500;
            }
            UIHandler.drawWin();
            this.winCounter--;
            if(this.winCounter <= 10) {
                this.winCounter = 0;
                this.state = "reloading";
            }
        }
        this.amIWorking = false;
    }
};

var upgradeHandler = {
    //contains upgrades objects
    upgradeList: new Array(), 
    //contains names of upgrades that effects later
    upgradeToCome: new Array(), 
    mouseOn: undefined,
    newUpgrade: false,

    /*  list upgrades:

    */
    
    /** 
     * @nocollapse 
     * */
    initialize() {
        this.upgradeList = new Array();
        this.upgradeToCome = new Array();
        this.mouseOn = undefined;
        this.newUpgrade = false;
        //to add new Upgrades
        //this.upgradeList.push(new Upgrade(name, cost, costAug, max, now, text, tooltip, img));
    },
    /**
     * recreate list of upgrades after loading (to transform anonym objects from JSON.parse() into Upgrade objects)
     * @nocollapse
     */
    reload() {
        let newList = new Array();
        for(let i=0; i<this.upgradeList.length; i++) {
            let u = this.upgradeList[i];
            let u2 = new Upgrade(u.name, u.cost, u.costAugmentation, u.max, u.now, u.text, u.tooltip, u.img);
            u2.current = u.current;
            u2.visible = u.visible;
            u2.visibleRatio = u.visibleRatio;
            newList.push(u2);
        }
        this.upgradeList = newList;
    },
    /**
     * check if enough currency to buy  
     * update currency  
     * effect now or place info for later  
     * @param {Upgrade} upgrade 
     * @nocollapse
     */
    buy(upgrade) {
        if(upgrade.cost <= 1 /*gameHandler.money*/) {
            //here change currency handling as fit
            //gameHandler.money -= upgrade.cost;
            upgrade.buy();
            if(upgrade.now)
                this.effect(upgrade.name);
            else   
                this.upgradeToCome.push({name: upgrade.name, text: upgrade.text});
                //here can change pushing method to count {name, text, count}
        }
    },
    /**
     * switch the upgrade names  
     * effect accordingly
     * @param {string} name 
     * @nocollapse
     */
    effect(name) {
        switch(name) {
            default:
                break;
        }
    },
    /**
     * effect the upgrades in the upgradeToCome list
     * @nocollapse
     */
    effectUpToCome() {
        for(let i=0; i<this.upgradeToCome.length; i++) {
            this.effect(this.upgradeToCome[i].name);
        }
        this.upgradeToCome = new Array();
    },
    /**
     * check all upgrades to see which to display
     * @returns {Array} of Upgrades
     * @nocollapse
     */
    listBuyables() {
        return this.upgradeList.filter(e => e.visible);
    },
    /**
     * check the list of upgrades against currency  
     * set this.newUpgrade flag to true if an upgrade becomes visible
     * @nocollapse
     */
    actualiseVisibility() {
        for(let i=0; i<this.upgradeList.length; i++) {
            let up = this.upgradeList[i];
            //here change for the currency employed
            let gold = 1; //gameHandler.money;
            if(!up.visible && up.current < up.max && up.cost*up.visibleRatio < gold) {
                up.visible = true;
                //flag that there is a new upgrade in town
                this.newUpgrade = true;
            }
        }
    },
    /** 
     * @nocollapse 
     * */
    draw() {
        
    },
};

var soundHandler = {
    musicOn: false,

    /** 
     * @nocollapse 
     * */
    initialize() {
        //start on
        this.musicOn = false;
        this.initializeSound();
    },
    /** 
     * @nocollapse 
     * */
    initializeSound() {
        /*
        this.sourceMusic = new Audio("sound/music1genericbackgroundtrack.mp3");
        this.sourceMusic.loop = true;
        this.sourceMusic.load();
        this.sourceMusic.addEventListener("load", function() {
            gameHandler.assetsLoading();
        }, true);
        */
    },
    /** 
     * @nocollapse 
     * */
    changeHearabilityMusic() {
        this.musicOn = !this.musicOn;
        this.actualiseMusic();
        UIHandler.drawMusicOnOff();
    },
    /** 
     * @nocollapse 
     * */
    actualiseMusic() {
        if(gameHandler.state != "loading") {
            if(this.musicOn) {
                //this.sourceMusic.play();
            } else {
                //this.sourceMusic.pause();
            }
        }
    }
};

var saveHandler = {
    file: undefined,
    countToSave: 500, //how many frames before automatic save
    importOpen: false,

    /** 
     * @nocollapse 
     * */
    load() {
        let save = window.localStorage.getItem("robin-2021-sovietMarket");
        if(save !== null) {
            this.file = JSON.parse(save);
            if(this.verifySave()) {
                //upgrades
                Object.assign(upgradeHandler.upgradeList, this.file.upgrade.upgradeList);
                Object.assign(upgradeHandler.upgradeToCome, this.file.upgrade.upgradeToCome);
                upgradeHandler.newUpgrade = this.file.upgrade.newUpgrade;
                //sound
                soundHandler.musicOn = this.file.sound.musicOn;
                //UI
                Object.assign(UIHandler.button, this.file.UI.button);
                //product
                productHandler.reload(this.file.product.productList);
                //market
                Object.assign(marketHandler.shelves, this.file.market.shelves);
                Object.assign(marketHandler.stockPos, this.file.market.stockPos);
                Object.assign(marketHandler.stockDest, this.file.market.stockDest);
                Object.assign(marketHandler.exitPos, this.file.market.exitPos);
                Object.assign(marketHandler.exitDest, this.file.market.exitDest);
                Object.assign(marketHandler.registerPos, this.file.market.registerPos);
                Object.assign(marketHandler.registerDest, this.file.market.registerDest);
                marketHandler.x = this.file.market.x;
                marketHandler.y = this.file.market.y;
                marketHandler.w = this.file.market.w;
                marketHandler.h = this.file.market.h;
                marketHandler.wallWidth = this.file.market.wallWidth;
                //customer
                Object.assign(customerHandler.customersUsed, this.file.customer.customersUsed);
                customerHandler.maxCustomerInMarket = this.file.customer.maxCustomerInMarket;
                customerHandler.chanceToSpawnCustomer = this.file.customer.chanceToSpawnCustomer;
                customerHandler.maxTickets = this.file.customer.maxTickets;
                customerHandler.clicGulag = this.file.customer.clicGulag;
                customerHandler.automaticGulag = this.file.customer.automaticGulag;
                //employee
                Object.assign(employeeHandler.employees, this.file.employee.employees);
                //tickets
                ticketHandler.tickets = this.file.ticket.tickets;
                //deals
                Object.assign(dealHandler.blackMarket, this.file.deal.blackMarket);
                Object.assign(dealHandler.governementMarket, this.file.deal.governementMarket);
                dealHandler.durationMin = this.file.deal.durationMin;
                dealHandler.durationMax = this.file.deal.durationMax;
                dealHandler.chanceToSpawn = this.file.deal.chanceToSpawn;
                dealHandler.deltaToCorrectPriceMin = this.file.deal.deltaToCorrectPriceMin;
                dealHandler.deltaToCorrectPriceMax = this.file.deal.deltaToCorrectPriceMax;
                dealHandler.quantityMin = this.file.deal.quantityMin;
                dealHandler.quantityMax = this.file.deal.quantityMax;
                dealHandler.govDealUnlocked = this.file.deal.govDealUnlocked;
                dealHandler.bmDealUnlocked = this.file.deal.bmDealUnlocked;
                dealHandler.automaticDeals = this.file.deal.automaticDeals;
                //ideas
                Object.assign(ideaHandler.ideaList, this.file.idea.ideaList);
                //employment
                Object.assign(employmentHandler.workStation, this.file.employment.workStation);
                employmentHandler.employmentUnlocked = this.file.employment.employmentUnlocked;
                employmentHandler.ungulaged = this.file.employment.ungulaged;
                employmentHandler.countToEmploy = this.file.employment.countToEmploy;

                upgradeHandler.reload();
                UIHandler.reload();
                marketHandler.reload();
                customerHandler.reload();
                employeeHandler.reload();
                dealHandler.reload();
                ideaHandler.reload();
                employmentHandler.reload();

                gameHandler.state = "reloading";
            }
            this.createFile();
        }
    },
    /** 
     * @nocollapse 
     * */
    save() {
        this.countToSave--;
        if(this.countToSave <= 0) {
            this.countToSave = 500;
            this.createFile();
            window.localStorage.setItem("robin-2021-sovietMarket", JSON.stringify(this.file));
        }
    },
    /** 
     * @nocollapse 
     * */
    createFile() {
        this.file = {
            "game": {

            },
            "UI": {
                "button": UIHandler.button,
            },
            "product": {
                "productList": productHandler.productList,
            },
            "market": {
                "shelves": marketHandler.shelves,
                "stockPos": marketHandler.stockPos,
                "stockDest": marketHandler.stockDest,
                "exitPos": marketHandler.exitPos,
                "exitDest": marketHandler.exitDest,
                "registerPos": marketHandler.registerPos,
                "registerDest": marketHandler.registerDest,
                "x": marketHandler.x,
                "y": marketHandler.y,
                "w": marketHandler.w,
                "h": marketHandler.h,
                "wallWidth": marketHandler.wallWidth,
            },
            "customer": {
                "customersUsed": customerHandler.customersUsed,
                "maxCustomerInMarket": customerHandler.maxCustomerInMarket,
                "chanceToSpawnCustomer": customerHandler.chanceToSpawnCustomer,
                "maxTickets": customerHandler.maxTickets,
                "clicGulag": customerHandler.clicGulag,
                "automaticGulag": customerHandler.automaticGulag,
            },
            "employee": {
                "employees": employeeHandler.employees,
            },
            "upgrade": {
                "upgradeList": upgradeHandler.upgradeList,
                "upgradeToCome": upgradeHandler.upgradeToCome,
                "newUpgrade": upgradeHandler.newUpgrade,
            },
            "sound": {
                "musicOn": soundHandler.musicOn,
            },
            "ticket": {
                "tickets": ticketHandler.tickets,
            },
            "deal": {
                "blackMarket": dealHandler.blackMarket,
                "governementMarket": dealHandler.governementMarket,
                "durationMin": dealHandler.durationMin,
                "durationMax": dealHandler.durationMax,
                "chanceToSpawn": dealHandler.chanceToSpawn,
                "deltaToCorrectPriceMin": dealHandler.deltaToCorrectPriceMin,
                "deltaToCorrectPriceMax": dealHandler.deltaToCorrectPriceMax,
                "quantityMin": dealHandler.quantityMin,
                "quantityMax": dealHandler.quantityMax,
                "govDealUnlocked": dealHandler.govDealUnlocked,
                "bmDealUnlocked": dealHandler.bmDealUnlocked,
                "automaticDeals": dealHandler.automaticDeals,
            },
            "idea": {
                "ideaList": ideaHandler.ideaList,
            },
            "employment": {
                "employmentUnlocked": employmentHandler.employmentUnlocked,
                "workStation": employmentHandler.workStation,
                "ungulaged": employmentHandler.ungulaged,
                "countToEmploy": employmentHandler.countToEmploy,
            }
        };
    },
    /** 
     * @nocollapse 
     * */
    erase() {
        window.localStorage.removeItem("robin-2021-sovietMarket");
        gameHandler.state = "reloading";
        ctxUI.clearRect(0,0,canvas.width, canvas.height);
        gameHandler.initialize();
    },
    /**
     * @returns {boolean}
     * @nocollapse
     */
    verifySave() {
        return (
            this.file !== undefined && 
            this.file !== null && 
            this.file.game !== undefined && 
            this.file.upgrade !== undefined && 
            this.file.sound !== undefined &&
            this.file.UI !== undefined &&
            this.file.product !== undefined &&
            this.file.market !== undefined &&
            this.file.customer !== undefined &&
            this.file.employee !== undefined &&
            this.file.ticket !== undefined &&
            this.file.deal !== undefined &&
            this.file.idea !== undefined &&
            this.file.employment !== undefined
            );
    },
    /** 
     * @nocollapse 
     * */
    import() {
        if(this.importOpen) {
            let save = contentImportExport.value;
            try {
                this.file = JSON.parse(window.atob(save));
                if(this.verifySave()) {
                    window.localStorage.setItem("robin-2021-sovietMarket", JSON.stringify(this.file));
                    this.countToSave = 500;
                    this.load();
                    this.openImportExport("Save successfully imported");
                }
            } catch(err) {
                contentImportExport.value = err;
            }
        } else {
            this.openImportExport("Import");
            this.importOpen = true;
        }
    },
    /** 
     * @nocollapse 
     * */
    export() {
        this.createFile();
        this.openImportExport("Copied to clipboard");
        let str = window.btoa(JSON.stringify(this.file));
        contentImportExport.value = str;
        contentImportExport.select();
        document.execCommand("copy");
    },
    /**
     * @param {string} message 
     * @nocollapse
     */
    openImportExport(message) {
        importExport.className = "notHidden";
        infoImportExport.textContent = message;
    },
    /** 
     * @nocollapse 
     * */
    closeImportExport() {
        importExport.className = "hidden";
        this.importOpen = false;
        contentImportExport.value = "Paste here your code, then click import";
        UIHandler.importExportOpen = false;
    },
};

var UIHandler = {
    button: new Array(),
    newResources: true,
    tooltipDisplay: "none",
    tooltipPos: {x:0,y:0},
    menuOpen: "ideas",
    menuOption: "none",
    eraseSave: false,
    importExportOpen: false,

    /** 
     * @nocollapse 
     * */
    initialize() {
        this.importExportOpen = false;
        this.eraseSave = false;
        this.menuOption = "none";
        this.tooltipDisplay = "none";
        this.tooltipPos = {x:0,y:0};
        this.button = new Array();
        this.newResources = true;
        this.menuOpen = "ideas";
        let button;
        //button = new Button(x,y,w,h,clickable,color,ctxParam,visible, effect);
        //button.addText(text, fontSize, fontColor, x, y, maxWidth);
        //this.button.push(button);
        button = new Button(5,5,290,70,false,"red",ctxUI,true);
        button.addText("Red Market", "50px kenney_miniregular", "yellow", 20, 55, 265);
        this.button.push(button);
        button = new Button(305,5,690,690,false,"invisible",ctxUI,true);
        this.button.push(button);
        button = new Button(5,155,290,540,false,"lightgrey",ctxUI,true);
        this.button.push(button);
        button = new Button(5,85,290,60,false,"yellow2",ctxUI,true);
        button.addText("Tickets:", "30px kenney_miniregular", "black", 20, 123, 100);
        this.button.push(button);
        button = new Button(1005,70,290,565,false,"lightgrey",ctxUI,true);
        this.button.push(button);
        button = new Button(1005,5,90,55,true,"red",ctxUI,true,"menuIdeas", "ideas");
        button.addText("Ideas", "35px kenney_miniregular", "black", 1020, 42, 60);
        this.button.push(button);
        button = new Button(1105,5,90,55,true,"red",ctxUI,false, "menuDeals", "deals");
        button.addText("Deals", "35px kenney_miniregular", "black", 1120, 42, 60);
        this.button.push(button);
        button = new Button(1205,5,90,55,true,"red",ctxUI,false,"menuEmploy", "employ");
        button.addText("Employ", "35px kenney_miniregular", "black", 1220, 42, 60);
        this.button.push(button);
    },
    /**
     * @nocollapse
     */
    reload() {
        let newButtons = new Array();
        for(let i=0; i<this.button.length; i++) {
            let butt = this.button[i];
            let newButt = new Button(butt.x,butt.y,butt.w,butt.h,butt.clickable,"red",ctxUI,butt.visible,butt.effect);
            newButt.color = butt.color;
            newButt.name = butt.name;
            for(let j=0; j<butt.textList.length; j++) {
                let t = butt.textList[j];
                newButt.textList.push({"text": t.text, "fontSize": t.fontSize, "fontColor": t.fontColor, "x": t.x, "y": t.y, "maxWidth": t.maxWidth});
            }
            newButtons.push(newButt);
        }
        this.button = newButtons;
        this.newResources = true; //to display again
    },
    /** 
     * @nocollapse 
     * */
    clic() {
        let x = mouse.x;
        let y = mouse.y;
        let i=0;
        //to stop when a clic is performed
        let clicEffect = false;
        if(this.importExportOpen) {
            clicEffect = true;
        }
        if(!clicEffect && this.menuOption !== "none") {
            if(this.clicOnOption(x,y))
                clicEffect = true;
        }
        //if clic to stop sales
        if(!clicEffect && (this.clicToStopSales(x,y) || this.clicToChangePrice(x,y))) {
            clicEffect = true;
            this.newResources = true;
        }
        while(i<this.button.length && !clicEffect) {
            let effectName = this.button[i].clic(x,y);
            if(effectName !== undefined) {
                //here effect the clic
                switch(effectName) {
                    case "menuDeals":
                        this.openMenu("deals");
                        break;
                    case "menuIdeas":
                        this.openMenu("ideas");
                        break;
                    case "menuEmploy":
                        this.openMenu("employ");
                        break;
                }
                clicEffect = true;
            }
            i++;
        }
        if(!clicEffect && this.menuOpen === "deals") {
            clicEffect = dealHandler.clic(x,y);
        }
        if(!clicEffect && this.menuOpen === "ideas") {
            clicEffect = ideaHandler.clic(x,y);
        }
        if(!clicEffect && this.menuOpen === "employ") {
            clicEffect = employmentHandler.clic(x,y);
        }
        if(!clicEffect && y >= 640 && y <= 690) {
            if(x >= 1120 && x <= 1170) {
                soundHandler.changeHearabilityMusic();
            } else if(x >= 1180 && x <= 1230) {
                this.openCloseOption("help");
            } else if(x >= 1240 && x <= 1290) {
                this.openCloseOption("option");
            }
        }
        if(!clicEffect) {
            if(customerHandler.clic(x,y))
                clicEffect = true;
        }
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if clic found
     */
    clicOnOption(x,y) {
        if(x >= 1050 && x <= 1090 && y >= 110 && y <= 150) {
            this.openCloseOption("none");
            return true;
        }
        if(this.menuOption === "option") {
            if(x >= 300 && x <= 1000 && y >= 520 && y <= 590) {
                if(this.eraseSave) {
                    saveHandler.erase();
                } else {
                    this.eraseSave = true;
                }
            } else if(y >= 450 && y <= 500) {
                if(x >= 300 && x <= 600) {
                    this.importExportOpen = true;
                    saveHandler.import();
                    return true;
                }
                if(x >= 700 && x <= 1000) {
                    this.importExportOpen = true;
                    saveHandler.export();
                    return true;
                }
            }
        }
        if(x >= 200 && x <= 1100 && y >= 100 && y <= 600)
            return true;
        return false;
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y 
     */
    hover(x,y) {
        let notFound = true;
        if(this.menuOpen === "deals" && dealHandler.hover(x,y)) {
            notFound = false;
        }
        if(notFound && this.menuOpen === "ideas") {
            if(ideaHandler.hover(x,y)) {
                notFound = false;
            }
        }
        if(notFound && this.menuOpen === "employ") {
            if(employmentHandler.hover(x,y)) {
                notFound = false;
            }
        }
        if(notFound) {
            const offsetY = 165;
            const offsetX = 75;
            const distY = 35;
            const w = 32;
            const h = 32;
            if(x >= offsetX && x <= offsetX + w) {
                let resourcesList = productHandler.getProductNameAndQuantity();
                let prodNames = Object.keys(resourcesList);
                for(let i=0; i<prodNames.length; i++) {
                    if(notFound && y >= offsetY+distY*i && y <= offsetY+h+distY*i) {
                        notFound = false;
                        this.tooltipDisplay = prodNames[i];
                        this.tooltipPos.x = offsetX+w;
                        this.tooltipPos.y = offsetY+h/2+distY*i;
                    }
                }
            }
            if(notFound) {
                this.tooltipDisplay = "none";
            }
        }
    },
    /**
     * @nocollapse
     * @param {string} option "option"/"help"
     */
    openCloseOption(option) {
        if(this.menuOption === "none" || this.menuOption !== option) {
            this.menuOption = option;
        } else {
            this.menuOption = "none";
        }
        this.eraseSave = false;
    },
    /**
     * @nocollapse
     */
    drawLoading() {
        ctxFront.fillStyle = "white";
        ctxFront.fillRect(0,0,canvas.width, canvas.height);
        ctxFront.font = "70px kenney_miniregular";
        ctxFront.fillStyle = "black";
        ctxFront.fillText("Loading ("+gameHandler.assetsLoaded+"/"+Object.keys(img).length+")", 380, 350);
    },
    /**
     * @nocollapse
     */
    drawLost() {
        ctxFront.fillStyle = "rgba(240, 52, 52, 1)";
        ctxFront.fillRect(0,0,canvas.width, canvas.height);
        ctxFront.fillStyle = "black";
        ctxFront.font = "70px kenney_miniregular";
        ctxFront.fillText("You've been sent to gulag", 120, 350);
        ctxFront.font = "50px kenney_miniregular";
        ctxFront.fillText("Your save will be erased in "+ticksToText(gameHandler.loseCounter), 380, 420);
    },
    /**
     * @nocollapse
     */
    drawWin() {
        ctxFront.fillStyle = "rgba(240, 52, 52, 1)";
        ctxFront.fillRect(0,0,canvas.width, canvas.height);
        ctxFront.fillStyle = "black";
        ctxFront.font = "70px kenney_miniregular";
        ctxFront.fillText("You successfully crossed the border", 100, 350, 1100);
        ctxFront.font = "50px kenney_miniregular";
        ctxFront.fillText("You can continue playing in "+ticksToText(gameHandler.winCounter), 380, 420);
    },
    /**
     * @nocollapse
     * @param {string} prodName 
     * @param {number} x 
     * @param {number} y 
     */
    drawTooltip(prodName, x, y) {
        const w = 170;
        const h = 20;
        const off = 3;
        ctxFront.lineWidth = 3;
        ctxFront.strokeStyle = "black";
        ctxFront.fillStyle = "white";
        ctxFront.beginPath();
        ctxFront.moveTo(x,y);
        ctxFront.lineTo(x+off,y-off);
        ctxFront.lineTo(x+off,y-h);
        ctxFront.lineTo(x+w,y-h);
        ctxFront.lineTo(x+w,y+h);
        ctxFront.lineTo(x+off,y+h);
        ctxFront.lineTo(x+off,y+off);
        ctxFront.lineTo(x,y);
        ctxFront.stroke();
        ctxFront.fill();
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        ctxFront.fillText(prodName, x+5, y+10, w-10);
    },
    /**
     * @nocollapse
     */
    drawSave() {
        if(saveHandler.countToSave > 460) {
            ctxFront.drawImage(img.save,1060,640,50,50);
        }
    },
    /**
     * @nocollapse
     */
    drawMusicOnOff() {
        ctxUI.clearRect(1120,640,50,50);
        let music;
        if(soundHandler.musicOn)
            music = img.musicOn;
        else
            music = img.musicOff;
        ctxUI.drawImage(music,1120,640,50,50);
    },
    /** 
     * @nocollapse 
     * */
    draw() {
        for(let i=0; i<this.button.length; i++) {
            this.button[i].draw();
        }
        this.openMenu(this.menuOpen); //first draw of the menu
        //buttons options right bottom
        this.drawMusicOnOff();
        ctxUI.drawImage(img.help,1180,640,50,50);
        ctxUI.drawImage(img.option,1240,640,50,50);
    },
    /**
     * @nocollapse
     */
    drawInfos() {
        let resourcesList = productHandler.getProductNameAndQuantity();
        let prodNames = Object.keys(resourcesList);
        if(this.newResources) {
            ctxInfos.clearRect(5,85,290,610);
            this.drawResourceList(prodNames);
            this.newResources = false;
        }
        const offsetY = 165;
        const offsetX = 20;
        const offsetYText = 27;
        const distY = 35;
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        for(let i=0; i<prodNames.length; i++) {
            ctxFront.fillText(resourcesList[prodNames[i]], offsetX, offsetY+offsetYText+distY*i,50);
        }
        let tickets = ticketHandler.getTickets().toString();
        for(let i=tickets.length-1; i>=0; i--) {
            ctxFront.fillText(tickets[i], 260-(tickets.length-1-i)*20, 123, 20);
        }
        if(this.tooltipDisplay !== "none") {
            this.drawTooltip(this.tooltipDisplay, this.tooltipPos.x, this.tooltipPos.y);
        }
        if(this.menuOpen === "deals") {
            dealHandler.drawDeals();
        } else if(this.menuOpen === "ideas") {
            ideaHandler.drawIdeas();
        } else if(this.menuOpen === "employ") {
            employmentHandler.drawStation();
        }
        if(saveHandler.countToSave > 460) {
            this.drawSave();
        }
        if(this.menuOption !== "none") {
            this.drawOptions();
        }
    },
    /**
     * @nocollapse
     */
    drawOptions() {
        ctxFront.strokeStyle = "black";
        ctxFront.lineWidth = 10;
        ctxFront.fillStyle = "rgba(245, 215, 110, 1)";
        ctxFront.fillRect(200,100,900,500);
        ctxFront.strokeRect(200,100,900,500);
        ctxFront.strokeStyle = "rgba(240, 52, 52, 1)";
        ctxFront.beginPath();
        ctxFront.moveTo(1050,110);
        ctxFront.lineTo(1090,150);
        ctxFront.moveTo(1090,110);
        ctxFront.lineTo(1050,150);
        ctxFront.stroke();
        ctxFront.fillStyle = "black";
        ctxFront.font = "50px kenney_miniregular";
        if(this.menuOption === "option") {
            ctxFront.fillText("SOVIET MARKET", 220, 150);
            ctxFront.font = "40px kenney_miniregular";
            ctxFront.fillText("August 2021 - Code by Robin", 220, 210);
            ctxFront.fillText("People img by Kenney", 220,270);
            ctxFront.fillText("www.kenney.nl", 270,320);
            //ctxFront.fillText("Music by Anttis", 220,370);
            //ctxFront.fillText("anttismusic.blogspot.com", 270,420);
            ctxFront.lineWidth = 5;
            //import/export
            ctxFront.strokeStyle = "black";
            ctxFront.strokeRect(300,450,300,50);
            ctxFront.strokeRect(700,450,300,50);
            ctxFront.fillText("Import", 385, 485);
            ctxFront.fillText("Export", 785, 485);
            //erase
            ctxFront.fillStyle = "rgba(240, 52, 52, 1)";
            ctxFront.strokeStyle = "rgba(240, 52, 52, 1)";
            ctxFront.strokeRect(300,520,700,70)
            if(this.eraseSave) {
                ctxFront.fillText("Are you sure?", 510, 565);
            } else {
                ctxFront.fillText("Reset the game (will erase save)", 315, 565);
            }
        } else if(this.menuOption === "help") {
            ctxFront.fillText("HELP", 220, 150);
            ctxFront.font = "40px kenney_miniregular";
            ctxFront.fillText("-> You can click on product image to stop restocking", 220, 210, 860);
            ctxFront.fillText("-> If products are too expensive, nobody can buy it", 220, 270, 860);
            ctxFront.fillText("-> Sometimes, if products are priced high", 220, 330, 660);
            ctxFront.fillText("customers will try to rob it", 260, 380, 480);
            ctxFront.fillText("-> You only recuperate the products", 220, 440, 660);
            ctxFront.fillText("if you click (not on automatic gulag)", 260, 490, 660);
            ctxFront.fillStyle = "white";
            ctxFront.strokeStyle = "black";
            ctxFront.lineWidth = 5;
            ctxFront.fillRect(920,320,50,50);
            ctxFront.strokeRect(920,320,50,50);
            ctxFront.drawImage(img.rob, 929, 329, 32, 32);
        }
    },
    /**
     * @param {Array} prodNames
     * @nocollapse
     */
    drawResourceList(prodNames) {
        const offsetY = 165;
        const offsetX = 75;
        const offsetYText = 27;
        const distY = 35;
        const w = 32;
        const h = 32;
        ctxInfos.fillStyle = "black";
        ctxInfos.font = "30px kenney_miniregular";
        for(let i=0; i<prodNames.length; i++) {
            let image = productHandler.getProductImg32(prodNames[i]);
            ctxInfos.drawImage(image, offsetX, offsetY+distY*i,w,h);
            //ctxInfos.fillText(prodNames[i],offsetX+40,offsetY+offsetYText+distY*i, 150);
            if(!productHandler.getProduct(prodNames[i]).sellIt) {
                ctxInfos.strokeStyle = "black";
                ctxInfos.lineWidth = 5;
                ctxInfos.beginPath();
                ctxInfos.moveTo(offsetX, offsetY+distY*i);
                ctxInfos.lineTo(offsetX+w, offsetY+h+distY*i);
                ctxInfos.moveTo(offsetX+w, offsetY+distY*i);
                ctxInfos.lineTo(offsetX, offsetY+h+distY*i);
                ctxInfos.stroke();
            }
            ctxInfos.fillText(productHandler.getProductPrice(prodNames[i])+" "+tcross, offsetX+40, offsetY+offsetYText+distY*i, 80);
            ctxInfos.drawImage(img.down, offsetX+110, offsetY+10+distY*i,16,16);
            ctxInfos.drawImage(img.up, offsetX+130, offsetY+10+distY*i,16,16);
            let emotion = productHandler.getProduct(prodNames[i]).getEmotionDueToPrice();
            if(emotion < -2)
                emotion = -2;
            if(emotion > 2)
                emotion = 2;
            switch(emotion) {
                case -2:
                    image = img.very_unhappy;
                    break;
                case -1:
                    image = img.unhappy;
                    break;
                case 0:
                    image = img.no_emotion;
                    break;
                case 1:
                    image = img.happy;
                    break;
                case 2:
                    image = img.very_happy;
                    break;
            }
            ctxInfos.drawImage(image, offsetX+160,offsetY+10+distY*i,16,16);
        }
    },
    /**
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if clic on one of images
     */
    clicToStopSales(x,y) {
        const offsetY = 165;
        const offsetX = 75;
        const distY = 35;
        const w = 32;
        const h = 32;        
        let notFound = true;
        if(x >= offsetX && x <= offsetX + w) {
            let resourcesList = productHandler.getProductNameAndQuantity();
            let prodNames = Object.keys(resourcesList);
            for(let i=0; i<prodNames.length; i++) {
                if(notFound && y >= offsetY+distY*i && y <= offsetY+h+distY*i) {
                    notFound = false;
                    let prod = productHandler.getProduct(prodNames[i]);
                    if(prod.sellIt)
                        prod.stopSales();
                    else
                        prod.beginSales();
                }
            }
        }
        return !notFound;
    },
    /**
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if clic on up or down to change price
     */
    clicToChangePrice(x,y) {
        const offsetY = 175;
        const offsetX = 185;
        const distY = 35;
        const distX = 20;
        const w = 16;
        const h = 16;
        let notFound = true;
        if((x >= offsetX && x <= offsetX + w) || (x >= offsetX + distX && x <=  offsetX + distX + w)) {
            let resourcesList = productHandler.getProductNameAndQuantity();
            let prodNames = Object.keys(resourcesList);
            for(let i=0; i<prodNames.length; i++) {
                if(notFound && y >= offsetY+distY*i && y <= offsetY+h+distY*i) {
                    notFound = false;
                    let prod = productHandler.getProduct(prodNames[i]);
                    if(x <= offsetX + w)
                        prod.decreasePrice();
                    else
                        prod.augmentPrice();
                }
            }
        }
        return !notFound;
    },
    /**
     * @nocollapse
     * @param {string} menu "deals"
     */
    openMenu(menu) {
        this.menuOpen = menu;
        dealHandler.tooltipDisplay = false;
        ideaHandler.tooltipDisplay = false;
        employmentHandler.tooltipDisplay = false;
        switch(this.menuOpen) {
            case "deals":
                dealHandler.draw();
                break;
            case "ideas":
                ideaHandler.draw();
                break;
            case "employ":
                employmentHandler.draw();
                break;
        }
    },
    /**
     * @nocollapse
     * @param {string} tabName 
     */
    unlockTab(tabName) {
        let butt = this.button.filter(e => e.name === tabName);
        if(butt.length > 0) {
            butt[0].display();
            butt[0].draw();
        }
    }
};

var productHandler = {
    productList: new Array(),

    /** 
     * @nocollapse 
     * */
    initialize() {
        this.productList = new Array();
        let quantity = 0;
        //this.productList.push(new Product(name, img, quantity, correctPrice, secondary))
        this.productList.push(new Product("Potato", img.potato, 50, 1));
        this.productList.push(new Product("Wheat Flour", img.wheat_flour, quantity, 4));
        this.productList.push(new Product("Buckwheat Flour", img.buckwheat_flour, quantity, 2, true));
        this.productList.push(new Product("Seasonal Vegetables", img.seasonal_vegetables, quantity, 3));
        this.productList.push(new Product("Potluck Soup", img.potluck_soup, quantity, 1, true));
        this.productList.push(new Product("Soap", img.soap, quantity, 5));
        this.productList.push(new Product("Cow Milk", img.cow_milk, quantity, 4));
        this.productList.push(new Product("Skim Milk", img.skim_milk, quantity, 2, true));
        this.productList.push(new Product("Vodka", img.vodka, quantity, 6));
        this.productList.push(new Product("Rapeseed Oil", img.rapeseed_oil, quantity, 4));
        this.productList.push(new Product("Frying Oil", img.frying_oil, quantity, 2, true));
        this.productList.push(new Product("Beef Meat", img.beef_meat, quantity, 6));
        this.productList.push(new Product("Ground Beef", img.ground_beef, quantity, 4, true));
        this.productList.push(new Product("Great Leader Portrait", img.great_leader, quantity, 50));
        this.productList.push(new Product("Collectible Thumbnails", img.collectible_thumbnails, quantity, 2, true));

        this.productList[0].becomeVisible();
    },
    /**
     * @param {Array} prodList
     * @nocollapse
     */
    reload(prodList) {
        for(let i=0; i<prodList.length; i++) {
            let prod = prodList[i];
            let prodToChange = this.getProduct(prod.name);
            prodToChange.quantity = prod.quantity;
            prodToChange.visible = prod.visible;
            prodToChange.price = prod.price;
            prodToChange.correctPrice = prod.correctPrice;
            prodToChange.sellIt = prod.sellIt;
            prodToChange.secondary = prod.secondary;
        }
    },
    /**
     * for products stocked (not on shelves)
     * @param {string} productName 
     * @returns {number} quantity
     * @nocollapse
     */
    getQuantity(productName) {
        let product = this.productList.filter(e => e.name === productName);
        console.assert(product !== undefined && product !== null, productName);
        return product.getQuantity();
    },
    /**
     * @param {string} productName 
     * @returns {Product | undefined} product
     * @nocollapse
     */
    getProduct(productName) {
        let list = this.productList.filter(e => e.getName() === productName);
        if(list.length === 0)
            return undefined;
        return list[0];
    },
    /**
     * @param {boolean} saleAuthorized
     * @returns {Object<?,?>} {prodName1: quantity, prodName2: ...}
     * @nocollapse
     */
    getProductNameAndQuantity(saleAuthorized = false) {
        let list = {};
        for(let prod of this.productList) {
            if(prod.visible && (!saleAuthorized || prod.sellIt))
                list[prod.name] = prod.getQuantity();
        }
        return list;
    },
    /**
     * for all type of product
     * @param {string} productName 
     * @returns {Image}
     * @nocollapse
     */
    getProductImg(productName) {
        let product = this.productList.filter(e => e.name === productName)[0];
        console.assert(product !== undefined && product !== null, productName);
        return product?.getImg();
    },
    /**
     * @nocollapse
     * @param {string} productName
     * @returns {Image} image 32x32 
     */
    getProductImg32(productName) {
        let str = "cow_milk";
        switch(productName) {
            case "Cow Milk":
                str = "cow_milk";
                break;
            case "Rapeseed Oil":
                str = "rapeseed_oil";
                break;
            case "Potato":
                str = "potato";
                break;
            case "Wheat Flour":
                str = "wheat_flour";
                break;
            case "Buckwheat Flour":
                str = "buckwheat_flour";
                break;
            case "Seasonal Vegetables":
                str = "seasonal_vegetables";
                break;
            case "Potluck Soup":
                str = "potluck_soup";
                break;
            case "Soap":
                str = "soap";
                break;
            case "Skim Milk":
                str = "skim_milk";
                break;
            case "Vodka":
                str = "vodka";
                break;
            case "Frying Oil":
                str = "frying_oil";
                break;
            case "Beef Meat":
                str = "beef_meat";
                break;
            case "Ground Beef":
                str = "ground_beef";
                break;
            case "Great Leader Portrait":
                str = "great_leader";
                break;
            case "Collectible Thumbnails":
                str = "collectible_thumbnails";
                break;
        }
        str = str + "32";
        return img[str];
    },
    /**
     * @nocollapse
     * @param {string} productName
     * @returns {number} price 
     */
    getProductPrice(productName) {
        let prod = this.getProduct(productName);
        if(prod !== undefined)
            return prod.getPrice();
        else   
            return -1;
    },
    /**
     * @returns {Array} array of productnames (string)
     * @nocollapse
     */
    getProductNameList() {
        return this.productList.filter(e => e.visible).map(e => e.getName());
    },
    /**
     * @returns {Array} array of productnames (string)
     * @nocollapse
     */
    getPrimaryProductNameList() {
        return this.productList.filter(e => e.visible && !e.secondary).map(e => e.getName());
    },
    /**
    * @returns {Array} array of productnames (string) if quantity > 0
    * @nocollapse
    */
    getProductNameListAvailable() {
        return this.productList.filter(e => e.visible && e.quantity > 0).map(e => e.getName());
    },
    /**
     * @param {string} productName 
     * @param {number} quantity 
     * @nocollapse
     */
    addQuantityToProduct(productName, quantity) {
        this.getProduct(productName).add(quantity);
    },
    /**
     * @param {string} productName 
     * @param {number} quantity 
     * @nocollapse
     */
    subtractFromProduct(productName, quantity) {
        this.getProduct(productName).subtract(quantity);
    },
    /**
     * @nocollapse
     */
    increaseCorrectPrice() {
        for(let i=0; i<this.productList.length; i++) {
            this.productList[i].correctPrice++;
        }
        UIHandler.newResources = true;
    }
};

var marketHandler = {
    shelves: new Array(),
    stockPos: {x: 940, y: 148, w: 30, h: 30},
    stockDest: {x: 874, y: 132},
    exitPos: {x: 900, y: 640, w: 30, h: 30},
    exitDest: {x: 900, y: 546},
    registerPos: {x: 370, y: 50, w: 64, h: 64},
    registerDest: {x: 385, y: 114},
    x: 330,
    y: 30,
    w: 640,
    h: 640,
    wallWidth: 32,

    /** 
     * @nocollapse 
     * */
    initialize() {
        this.shelves = new Array();
        this.stockPos = {x: 940, y: 148, w: 30, h: 30};
        this.stockDest =  {x: 874, y: 132};
        this.exitPos = {x: 900, y: 640, w: 30, h: 30};
        this.exitDest = {x: 900, y: 546};
        this.registerPos = {x: 370, y: 50, w: 64, h: 64};
        this.registerDest = {x: 385, y: 114};
        this.x = 330;
        this.y = 30;
        this.w = 640;
        this.h = 640;
        this.wallWidth = 30;
        //shelves
        //this.shelves.push(new Shelf(x,y,posRetrieving));

        this.shelves.push(new Shelf(456,188,"left"));
        this.shelves.push(new Shelf(456,252,"left"));
        this.shelves.push(new Shelf(456,316,"left"));
        this.shelves.push(new Shelf(456,380,"left"));
        this.shelves.push(new Shelf(456,444,"left"));
        this.shelves.push(new Shelf(456,508,"left"));
        
        this.shelves.push(new Shelf(520,188,"right"));
        this.shelves.push(new Shelf(520,252,"right"));
        this.shelves.push(new Shelf(520,316,"right"));
        this.shelves.push(new Shelf(520,380,"right"));
        this.shelves.push(new Shelf(520,444,"right"));
        this.shelves.push(new Shelf(520,508,"right"));

        this.shelves.push(new Shelf(716,188,"left"));
        this.shelves.push(new Shelf(716,252,"left"));
        this.shelves.push(new Shelf(716,316,"left"));
        this.shelves.push(new Shelf(716,380,"left"));
        this.shelves.push(new Shelf(716,444,"left"));
        this.shelves.push(new Shelf(716,508,"left"));

        this.shelves.push(new Shelf(780,188,"right"));
        this.shelves.push(new Shelf(780,252,"right"));
        this.shelves.push(new Shelf(780,316,"right"));
        this.shelves.push(new Shelf(780,380,"right"));
        this.shelves.push(new Shelf(780,444,"right"));
        this.shelves.push(new Shelf(780,508,"right"));

/*
        this.shelves.push(new Shelf(488,252,"left"));
        this.shelves.push(new Shelf(488,316,"left"));
        this.shelves.push(new Shelf(552,252,"right"));
        //this.shelves.push(new Shelf(876,316,"left"));
        */
    },
    /**
     * @nocollapse
     */
    reload() {
        let newShelves = new Array();
        for(let i=0; i<this.shelves.length; i++) {
            let shelf = this.shelves[i];
            let newShelf = new Shelf(shelf.x, shelf.y, shelf.frontPosRetrieving);
            newShelf.productStocked = shelf.productStocked;
            newShelf.quantityStocked = shelf.quantityStocked;
            newShelf.maxStocked = shelf.maxStocked;
            newShelves.push(newShelf);
        }
        this.shelves = newShelves;
    },
    /**
     * draw on ctxBG
     * @nocollapse
     */
    drawBackground() {
        ctxBG.clearRect(0,0,canvas.width,canvas.height);
        //walls
        ctxBG.fillStyle = "rgba(0,0,0,1)";
        ctxBG.fillRect(this.x, this.y, this.w, this.h);
        ctxBG.fillStyle = "rgba(84,84,84,1)";
        ctxBG.fillRect(this.x+5, this.y+5, this.w-10, this.h-10);
        ctxBG.fillStyle = "rgba(0,0,0,1)";
        ctxBG.fillRect(this.x+this.wallWidth-5, this.y+this.wallWidth-5, this.w-this.wallWidth*2+10, this.h-this.wallWidth*2+10);
        ctxBG.fillStyle = "white";
        ctxBG.fillRect(this.x+this.wallWidth, this.y+this.wallWidth, this.w-this.wallWidth*2, this.h-this.wallWidth*2);
        //tiles
        for(let i=1+this.x+this.wallWidth;i<=16+this.x+this.w-2*this.wallWidth;i+=16) {
            for(let j=1+this.y+this.wallWidth;j<=16+this.y+this.h-2*this.wallWidth;j+=16) {
                ctxBG.drawImage(img.tile,i,j,16,16);
            }
        }
        //stockDoor
        ctxBG.drawImage(img.stock_door, this.stockPos.x, this.stockPos.y, this.stockPos.w, this.stockPos.h);
        //exitDoor
        ctxBG.drawImage(img.front_door, this.exitPos.x, this.exitPos.y, this.exitPos.w, this.exitPos.h);
        //register
        ctxBG.drawImage(img.register, this.registerPos.x, this.registerPos.y, this.registerPos.w, this.registerPos.h);
        //shelves
        for(let i=0; i<this.shelves.length; i++) {
            this.shelves[i].drawShelf();
            this.shelves[i].subtract(0); //to show content
        }
    },
    /** 
     * @nocollapse 
     * */
    getRegisterDest() {
        return this.registerDest;
    },
    /** 
     * @nocollapse 
     * */
    getExitDest() {
        return this.exitDest;
    },
    /** 
     * @nocollapse 
     * */
    getStockDest() {
        return this.stockDest;
    },
    /**
     * @param {string} productName 
     * @returns {Array} Array of shelves
     * @nocollapse
     */
    getShelvesThatContains(productName) {
        return this.shelves.filter(e => e.getProduct().name === productName && e.quantityStocked > 0);
    },
    /**
     * @param {string} productName 
     * @returns {Object | undefined} {dest: {x, y}, shelf: Shelf} or undefined if no shelf has this product
     * @nocollapse
     */
    getShelfDestToBuy(productName) {
        let pos = this.getShelvesThatContains(productName);
        if(pos.length > 0) {
            return {"dest": pos[0].getAccessPoint(), "shelf": pos[0]};
        } else {
            return undefined;
        }
    },
    /**
     * @returns {Array} array of shelves
     * @nocollapse
     */
    getEmptyShelves() {
        return this.shelves.filter(e => e.getProduct().name === "none");
    },
    /**
     * @returns {Object} dictionnary:  {name: quantity}
     * @nocollapse
     */
    getProductShelvedFormatted() {
        let dict = {};
        let listNames = productHandler.getProductNameList();
        for(let i=0; i<listNames.length; i++) { 
            dict[listNames[i]] = 0;
        }
        for(let i=0; i<this.shelves.length; i++) {
            let contain = this.shelves[i].getProduct();
            dict[contain.name] += contain.quantity;
        }
        return dict;
    },
    /**
     * @param {string} productName 
     * @param {number} quantity 
     * @returns {(Object|undefined)} {dest: {x, y}, quantity: num, shelf: Shelf} or undefined if impossible to find one 
     * @nocollapse
     */
    getShelfDestToAdd(productName, quantity) {
        let pos = this.getShelvesThatContains(productName).sort(compareShelvesByQuantityAddable);
        if(pos.length !== 0 && pos[0].getQuantityAddable() >= quantity) {
            return {"dest": pos[0].getAccessPoint(), "quantity": quantity, "shelf": pos[0]};
        }
        let empty = this.getEmptyShelves();
        if(empty.length !== 0) {
            return {"dest": empty[0].getAccessPoint(), "quantity": quantity, "shelf": empty[0]};
        }
        if(pos.length !== 0 && pos[0].getQuantityAddable() > 0) {
            return {"dest": pos[0].getAccessPoint(), "quantity": pos[0].getQuantityAddable(), "shelf": pos[0]};
        }
        return undefined;
    },
    /**
     * @param {number} x 
     * @param {number} y
     * @returns {Shelf} shelf on coordinates {x,y} 
     * @nocollapse
     */
    getShelfXY(x,y) {
        return this.shelves.filter(e => e.x === x && e.y === y)[0];
    },
    /**
     * @param {string} productName 
     * @param {number} quantity
     * @param {Shelf} shelf
     * @returns {number} quantity added to shelf 
     */
    addToShelf(productName, quantity, shelf) {
        let content = shelf.getProduct();
        if(content.name !== "none" && content.name !== productName)
            return 0;
        let addable = shelf.getQuantityAddable();
        if(addable < quantity) {
            shelf.addProduct(productName, addable);
            return addable;
        } else {
            shelf.addProduct(productName, quantity);
            return quantity;
        }
    },
    /**
     * @param {number} x 
     * @param {number} y
     * @param {number} w width
     * @param {number} h height
     * @returns {boolean} false or true 
     * @nocollapse
     */
    doICollide(x,y,w,h) {
        const offsetAcceptable = 4;
        //with walls
        if(x < this.x + this.wallWidth - offsetAcceptable || x + w > this.x + this.w - this.wallWidth + offsetAcceptable || y < this.y + this.wallWidth - offsetAcceptable || y + h > this.y + this.h - this.wallWidth + offsetAcceptable) {
            //console.log("wall");
            return true;
        }
        //with registers
        if(x < this.registerPos.x + this.registerPos.w - offsetAcceptable && x + w > this.registerPos.x + offsetAcceptable && y < this.registerPos.y + this.registerPos.h - offsetAcceptable && y + h > this.registerPos.y + offsetAcceptable) {
            //console.log("register", x, y, w, h);
            return true;
        }
        //with shelves
        for(let i=0; i<this.shelves.length; i++) {
            if(this.shelves[i].doICollide(x,y,w,h)){
                //console.log("shelf", i);
                return true;
            }
        }
        return false;
    }
};

var customerHandler = {
    customersNotUsed: new Array(),
    customersUsed: new Array(),
    maxCustomerInMarket: 0,
    chanceToSpawnCustomer: 0.05,
    maxTickets: 10,
    clicGulag: false,
    automaticGulag: false,

    /** 
     * @nocollapse 
     * */
    initialize() {
        this.clicGulag = false;
        this.automaticGulag = false;
        this.customersNotUsed = new Array();
        this.customersUsed = new Array();
        this.maxCustomerInMarket = 0;
        this.chanceToSpawnCustomer = 0.05;
        this.maxTickets = 10;
        for(let i=0; i<100; i++) {
            this.customersNotUsed.push(new Customer());
        }
    },
    /**
     * @nocollapse
     */
    reload() {
        let custList = this.customersUsed;
        this.customersUsed = new Array();
        for(let i=0; i<custList.length; i++) {
            this.spawnCustomer();
        }
        for(let i=0; i<custList.length; i++) {
            let oldCust = custList[i];
            let newCust = this.customersUsed[i];
            newCust.x = oldCust.x;
            newCust.y = oldCust.y;
            newCust.preferredProduct = oldCust.preferredProduct;
            newCust.maxTickets = oldCust.maxTickets;
            newCust.currentPriceToPay = oldCust.currentPriceToPay;
            newCust.direction = oldCust.direction;
            newCust.goalName = oldCust.goalName;
            newCust.transporting = JSON.parse(JSON.stringify(oldCust.transporting));
            newCust.goal = {};
            if(oldCust.goal !== undefined) {
                Object.assign(newCust.goal, oldCust.goal);
                if(oldCust.goal.shelf !== undefined)
                    newCust.goal.shelf = marketHandler.getShelfXY(oldCust.goal.shelf.x, oldCust.goal.shelf.y);
            } else {
                newCust.goal = undefined;
            }
        }
    },
    /** 
     * @nocollapse 
     * */
    draw() {
        for(let i=0; i<this.customersUsed.length; i++) {
            this.customersUsed[i].draw();
        }
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y
     * @returns {boolean} true if clic on a robber 
     */
    clic(x,y) {
        if(this.clicGulag === true) {
            let robList = this.customersUsed.filter(e => e.infoBubble === "rob");
            for(let i=0; i<robList.length; i++) {
                let rob = robList[i];
                if(x >= rob.x && x <= rob.x + rob.w && y >= rob.y && y <= rob.y + rob.h) {
                    this.gulag(rob, "clic");
                    return true;
                }
            }
        }
        return false;
    },
    /**
     * @nocollapse
     * @param {Customer} robber 
     * @param {string} meanOfArrestation "exit"/"clic"
     */
    gulag(robber, meanOfArrestation) {
        if(robber.infoBubble === "rob" && (meanOfArrestation === "clic" ||this.automaticGulag)) {
            //recuperate what he was transporting
            if(meanOfArrestation === "clic") {
                for(let i=0; i<robber.transporting.length; i++) {
                    productHandler.addQuantityToProduct(robber.transporting[i],1);
                }
            }
            //gulag it
            this.decayCustomer(robber);
            if(employmentHandler.employmentUnlocked) {
                employmentHandler.gulag();
            }
        }
    },
    /** 
     * @nocollapse 
     * */
    spawnCustomer() {
        let customer = this.customersNotUsed.pop();
        let products = productHandler.getProductNameList();
        let preferredProduct = products[Math.floor(Math.random()*products.length)];
        let maxTickets = Math.floor(Math.random()*this.maxTickets)+1;
        let startPos = marketHandler.getExitDest();
        customer.init(startPos.x,startPos.y,preferredProduct,maxTickets);
        this.customersUsed.push(customer);
    },
    /** 
     * @param {Customer} customer
     * @nocollapse 
     */
    decayCustomer(customer) {
        this.customersUsed = this.customersUsed.filter(e => e !== customer);
        this.customersNotUsed = this.customersNotUsed.filter(e => e !== customer); //strange bug 
        this.customersNotUsed.push(customer);
        customer.decay();
    },
    /**
     * Check if there is still a product on the goal shelf  
     * If true, take it, add to transporting, change shelf state  
     * @param {Customer} customer 
     */
    tryToBuy(customer) {
        let shelf = customer.getShelf();
        let product = shelf.getProduct();
        if(product.name !== "none") {
            let price = productHandler.getProductPrice(product.name);
            let tickets = customer.getTickets();
            if(product.quantity > 0 && price <= tickets) {
                shelf.subtract(1);
                customer.addToCart(product.name, price);
                customer.currentMood += productHandler.getProduct(product.name).getEmotionDueToPrice();
            }
        } else {
            customer.addBubble("unhappy");
        }
    },
    /**
     * Check if enough money to buy something not already transporting  
     * If true, set new goal  
     * Else, if not transporting anything, set exit goal  
     * Else, set register goal
     * @param {Customer} customer 
     */
    setProductGoal(customer) {
        //list of items available that the customer don't already have
        let productList = productHandler.getProductNameList();
        let cartList = customer.getCartList();
        if(cartList.length > 0) {
            for(const prod of cartList) {
                productList = productList.filter(e => e !== prod);
            }
        }
        //add prices to that list
        let ultList = {};
        if(productList.length > 0) {
            for(const prod of productList) {
                ultList[prod] = productHandler.getProductPrice(prod);
            }
        }
        let tickets = customer.getTickets();
        let chosenProduct = undefined;
        //check if one prod is buyable
        for(let prod in ultList) {
            if(chosenProduct === undefined && ultList[prod] <= tickets && marketHandler.getShelfDestToBuy(prod) !== undefined)
                chosenProduct = prod;
        }
        //if there is a chosen product go to it
        if(chosenProduct !== undefined) {
            let goal = marketHandler.getShelfDestToBuy(chosenProduct);
            if(goal !== undefined) {
                customer.setGoal(goal, "shelf");
                if(chosenProduct === customer.preferredProduct)
                    customer.addBubble(chosenProduct);
            }
        } else {
            //if not transporting anything, exit
            if(cartList.length === 0) {
                customer.setGoal({"dest": marketHandler.getExitDest()}, "exit");
                customer.addBubble("exit");
            } else {
                //go to register
                customer.setGoal({"dest": marketHandler.getRegisterDest()}, "register");
                customer.addBubble("register");
            }
        }
    },
    /**
     * move a customer
     * @param {Customer} customer 
     */
    moveCustomer(customer) {
        let start = customer.getPos();
        let goal = customer.getGoal();
        let speed = customer.getSpeed();
        let preferredDirection = customer.getPreferredDirection();
        let size = customer.getSize();
        let dir = path(start, goal, speed, preferredDirection, size);
        customer.move(dir);
    },
    /**
     * check if a customer has reached his goal
     * @param {Customer} customer 
     * @nocollapse
     */
    checkGoalReached(customer) {
        if(Math.abs(customer.x-customer.goal.dest.x) <= customer.speed && Math.abs(customer.y-customer.goal.dest.y) <= customer.speed) {
            switch(customer.goalName) {
                case "shelf":
                    this.tryToBuy(customer);
                    customer.reachGoal();
                    this.setProductGoal(customer);
                    break;
                case "register":
                    customer.reachGoal();
                    customer.setGoal({"dest": marketHandler.getExitDest()}, "exit");
                    customer.addBubble("exit");
                    customer.pay();
                    break;
                case "exit":
                    customer.reachGoal();
                    this.gulag(customer, "exit");
                    this.decayCustomer(customer);
                    break;
            }
        }
    },

    /** 
     * @nocollapse 
     * */
    core() {
        //spawn a customer
        if(this.customersUsed.length < this.maxCustomerInMarket && Math.random() < this.chanceToSpawnCustomer) {
            this.spawnCustomer();
        }
        for(let i=0; i<this.customersUsed.length; i++) {
            let customer = this.customersUsed[i];
            //if no goal, take one
            if(customer.goalName === undefined)
                this.setProductGoal(customer);
            //make customers move towards their goals
            this.moveCustomer(customer);
            //if goal attained : effect and change goal
            this.checkGoalReached(customer);
        }
    }
};

var employeeHandler = {
    employeesNotUsed: new Array(),
    employees: new Array(),

    /** 
     * @nocollapse 
     * */
    initialize() {
        this.employeesNotUsed = new Array();
        this.employees = new Array();
        for(let i=0; i<100; i++) {
            this.employeesNotUsed.push(new Employee());
        }

        //for test purpose
        this.spawnEmployee();
    },
    /**
     * @nocollapse
     */
    reload() {
        let empList = JSON.parse(JSON.stringify(this.employees));
        this.employees = new Array();
        for(let i=0; i<empList.length; i++) {
            this.spawnEmployee();
        }
        for(let i=0; i<empList.length; i++) {
            let oldEmp = empList[i];
            let newEmp = this.employees[i];
            newEmp.x = oldEmp.x;
            newEmp.y = oldEmp.y;
            newEmp.direction = oldEmp.direction;
            newEmp.goalName = oldEmp.goalName;
            newEmp.maxTransporting = oldEmp.maxTransporting;
            newEmp.reinitTransport();
            if(oldEmp.transporting !== undefined)
                Object.assign(newEmp.transporting, oldEmp.transporting);
            newEmp.goal = {};
            if(oldEmp.goal !== undefined) {
                Object.assign(newEmp.goal, oldEmp.goal);
                if(oldEmp.goal.shelf !== undefined)
                    newEmp.goal.shelf = marketHandler.getShelfXY(oldEmp.goal.shelf.x, oldEmp.goal.shelf.y);
            } else {
                newEmp.goal = undefined;
            }
        }
    },
    /** 
     * @nocollapse 
     * */
    draw() {
        for(let emp of this.employees) {
            if(emp.visible)
                emp.draw();
        }
    },
    /**
     * @nocollapse 
     * */
    spawnEmployee() {
        let employee = this.employeesNotUsed.pop();
        let startPos = marketHandler.getStockDest();
        employee.init(startPos.x,startPos.y);
        this.employees.push(employee);
    },
    /**
     * @nocollapse
     */
    unSpawnEmployee() {
        if(this.employees.length > 0) {
            let emp = this.employees[0];
            if(emp.getTransport().quantity !== undefined)
                this.restockStock(emp);
            this.decayEmployee(emp);
        }
    },
    /**
     * @param {Employee} employee 
     * @nocollapse
     */
    decayEmployee(employee) {
        this.employees = this.employees.filter(e => e !== employee);
        this.employeesNotUsed.push(employee);
        employee.decay();
    },
    /**
     * @param {Employee} employee 
     */
    setStockGoal(employee) {
        let stock = marketHandler.getStockDest();
        employee.setGoal({"dest": stock}, "stock");
    },
    /**
     * If transporting something, try to cram it on shelves  
     * -> if can't, go to stock  
     * Else choose what product to get from the stock:  
     * -> get stocks list  
     * -> try to shelve the most stocked item  
     * -> if not enough place (along a ratio), try the second, etc.  
     * -> then add transportation  
     * @param {Employee} employee 
     * @nocollapse
     */
    setProductGoal(employee) {
        let transporting = employee.getTransport();
        if(transporting.quantity !== undefined) {
            let goal = this.getProductGoal(transporting.productName, transporting.quantity);
            if(goal === undefined) {
                this.setStockGoal(employee);
            } else {
                employee.setGoal(goal, "shelf");
            }
        } else {
            let prodList = productHandler.getProductNameAndQuantity(true);
            //sort, most quantity in first
            prodList = Object.entries(prodList).sort(function(a,b) {return b[1]-a[1]});
            //prodList: [[name, quantity], [name, quantity] ...]
            let notFound = true;
            let i = 0;
            const ratio = 0.8;
            const capacity = employee.getRemainingTransportCapacity();
            while(notFound && i < prodList.length) {
                let productName = prodList[i][0];
                let quantity = Math.min(prodList[i][1], capacity);
                let goal = this.getProductGoal(productName, quantity);
                if(goal?.quantity >= ratio*quantity && quantity > 0) {
                    notFound = false;
                    //set goal
                    employee.setGoal(goal, "shelf");
                    //add transportation
                    employee.addTransporting(productName, goal.quantity);
                    //subtract from stock
                    productHandler.subtractFromProduct(productName, quantity);
                }
                i++;
            }
            if(notFound) {
                this.setStockGoal(employee);
            }
        }
    },
    /**
     * @param {string} productName 
     * @param {number} quantity 
     */
    getProductGoal(productName, quantity) {
        return marketHandler.getShelfDestToAdd(productName, quantity);
    },
    /**
     * @param {Employee} employee 
     */
    moveEmployee(employee) {
        let start = employee.getPos();
        let goal = employee.getGoal();
        let speed = employee.getSpeed();
        let preferredDirection = employee.getPreferredDirection();
        let size = employee.getSize();
        let dir = path(start, goal, speed, preferredDirection, size);
        employee.move(dir);
    },
    /**
     * check if enough place on shelf   
     * restock what you can  
     * @param {Employee} employee 
     */
    restockShelf(employee) {
        let shelf = employee.getShelf();
        if(shelf !== undefined) {
            let transporting = employee.getTransport();
            let added = marketHandler.addToShelf(transporting.productName, transporting.quantity, shelf);
            employee.subtractTransporting(added);
        }
    },
    /**
     * Empty hands into stock
     * @param {Employee} employee 
     */
    restockStock(employee) {
        let transporting = employee.getTransport();
        productHandler.addQuantityToProduct(transporting.productName, transporting.quantity);
        employee.reinitTransport();
    },
    /**
     * @param {Employee} employee 
     */
    checkGoalReached(employee) {
        if(Math.abs(employee.x-employee.goal.dest.x) <= employee.speed && Math.abs(employee.y-employee.goal.dest.y) <= employee.speed) {
            switch(employee.goalName) {
                case "shelf":
                    employeeHandler.restockShelf(employee);
                    employee.reachGoal();
                    if(employee.transporting.quantity !== undefined)
                        employeeHandler.setProductGoal(employee);
                    else
                        employeeHandler.setStockGoal(employee);
                    break;
                case "stock":
                    employee.reachGoal();
                    if(employee.transporting.quantity !== undefined)
                        employeeHandler.restockStock(employee);
                    employeeHandler.setProductGoal(employee);
                    if(employee.goalName === "stock")
                        employee.visible = false;
                    else 
                        employee.visible = true;
                    break;
            }
        }
    },

    /** 
     * @nocollapse 
     * */
    core() {
        for(let employee of this.employees) {
            //if no goal, take one
            if(employee.goalName === undefined)
                this.setStockGoal(employee);
            //if goal attained : effect and change goal
            this.checkGoalReached(employee);
            //make employees move towards their goals
            this.moveEmployee(employee);
        }
    }
};

var ticketHandler = {
    tickets: 0,

    /**
     * @nocollapse
     */
    initialize() {
        this.tickets = 0;
    },
    /**
     * @nocollapse
     * @param {number} quantity 
     */
    add(quantity) {
        this.tickets += quantity;
    },
    /**
     * @nocollapse
     * @param {number} quantity 
     * @returns {boolean} true if operation possible
     */
    subtract(quantity) {
        if(quantity > this.tickets) {
            return false;
        } else {
            this.tickets -= quantity;
            return true;
        }
    },
    /**
     * @nocollapse
     * @returns {number} tickets
     */
    getTickets() {
        return this.tickets;
    }
};

var dealHandler = {
    blackMarket: new Array(),
    governementMarket: new Array(),
    durationMin: 750,
    durationMax: 1500,
    chanceToSpawn: 0.05,
    deltaToCorrectPriceMin: 0,
    deltaToCorrectPriceMax: 10,
    quantityMax: 20,
    quantityMin: 2,
    maxDeals: 4,
    tooltipDisplay: false,
    govDealUnlocked: false,
    bmDealUnlocked: false,
    automaticDeals: false,

    /**
     * @nocollapse
     */
    initialize(){
        this.automaticDeals = false;
        this.govDealUnlocked = false;
        this.bmDealUnlocked = false;
        this.tooltipDisplay = false;
        this.blackMarket = new Array();
        this.governementMarket = new Array();
        this.durationMin = 750;
        this.durationMax = 1500;
        this.chanceToSpawn = 0.05;
        this.maxDeals = 4;
        this.deltaToCorrectPriceMin = 0;
        this.deltaToCorrectPriceMax = 10;
        this.quantityMax = 20;
        this.quantityMin = 2;
        //this.governementMarket.push(new GovDeal(prodName, quantity, cost, time, visible));
        this.governementMarket.push(new GovDeal("Wheat Flour", 25, 300, 45000, true));
        this.governementMarket.push(new GovDeal("Seasonal Vegetables", 50, 300, 45000, false));
        this.governementMarket.push(new GovDeal("Soap", 75, 750, 45000, false));
        this.governementMarket.push(new GovDeal("Cow Milk", 100, 800, 45000, false));
        this.governementMarket.push(new GovDeal("Vodka", 75, 900, 45000, false));
        this.governementMarket.push(new GovDeal("Rapeseed Oil", 250, 1000, 45000, false));        
        this.governementMarket.push(new GovDeal("Beef Meat", 100, 1200, 45000, false));
        this.governementMarket.push(new GovDeal("Great Leader Portrait", 25, 1250, 45000, false));
    },
    /**
     * @nocollapse
     */
    reload() {
        let bMarket = new Array();
        let gMarket = new Array();
        for(let i=0; i<this.blackMarket.length; i++) {
            let oldDeal = this.blackMarket[i];
            let newDeal = new Deal(oldDeal.prodName, oldDeal.quantity, oldDeal.cost, oldDeal.remainingTime);
            newDeal.startTime = oldDeal.startTime;
            newDeal.deprecated = oldDeal.deprecated;
            bMarket.push(newDeal);
        }
        for(let i=0; i<this.governementMarket.length; i++) {
            let oldDeal = this.governementMarket[i];
            let newDeal = new GovDeal(oldDeal.prodName, oldDeal.quantity, oldDeal.cost, oldDeal.remainingTime);
            newDeal.startTime = oldDeal.startTime;
            newDeal.visible = oldDeal.visible;
            gMarket.push(newDeal);
        }
        this.blackMarket = bMarket;
        this.governementMarket = gMarket;
    },
    /**
     * @nocollapse
     */
    draw() {
        ctxInfos.clearRect(1005,70,290,565);
        ctxInfos.fillStyle = "black";
        ctxInfos.font = "30px kenney_miniregular";
        ctxInfos.fillText("Government supply:", 1020, 105, 260);
        if(this.bmDealUnlocked)
            ctxInfos.fillText("Black Market:", 1020, 255, 260);
    },
    /**
     * @nocollapse
     */
    drawDeals() {
        const x = 1015;
        const y = 270;
        const distY = 90;
        for(let i=0; i<this.blackMarket.length; i++) {
            if(!this.blackMarket[i].deprecated)
                this.blackMarket[i].draw(x,y+i*distY);
        }
        for(let i=0; i<this.governementMarket.length; i++) {
            this.governementMarket[i].draw();
        }

        if(this.tooltipDisplay) {
            this.drawTooltip();
        }
    },
    /**
     * @nocollapse
     */
    drawTooltip() {
        const x = 1015;
        const y = 175;
        const w = 400;
        const h = 55;
        const off = 10;
        ctxFront.lineWidth = 3;
        ctxFront.strokeStyle = "black";
        ctxFront.fillStyle = "red";
        ctxFront.beginPath();
        ctxFront.moveTo(x,y);
        ctxFront.lineTo(x-off,y-off);
        ctxFront.lineTo(x-off,y-h);
        ctxFront.lineTo(x-w,y-h);
        ctxFront.lineTo(x-w,y+h);
        ctxFront.lineTo(x-off,y+h);
        ctxFront.lineTo(x-off,y+off);
        ctxFront.lineTo(x,y);
        ctxFront.stroke();
        ctxFront.fill();
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        ctxFront.fillText("Accepting this deal isn't obligatory", x-w+10, y-25, w-30);
        ctxFront.fillText("But the other option is the gulag", x-w+10, y+5, w-30);
        ctxFront.fillText("So it's strongly advised to accept it", x-w+10, y+35, w-30);
    },
    /**
     * @nocollapse
     */
    addDealMaybe() {
        let index = -1;
        for(let i=0; i<4; i++) {
            if(this.blackMarket[i].deprecated)
                index = i;
        }
        if(index !== -1) {
            if(Math.random() < this.chanceToSpawn)
                this.addDeal(index);
        }
    },
    /**
     * @nocollapse
     * @param {number} index
     */
    addDeal(index) {
        //get resource
        let prodList = productHandler.getPrimaryProductNameList();
        let prodName = prodList[Math.floor(Math.random()*prodList.length)];
        let prod = productHandler.getProduct(prodName);
        let img = productHandler.getProductImg(prodName);
        //get cost (around correct price, delta random)
        let price = prod.correctPrice + this.deltaToCorrectPriceMin + Math.floor(Math.random()*(1+Math.abs(this.deltaToCorrectPriceMin-this.deltaToCorrectPriceMax)));
        if(price < 1) {
            price = 1;
        }
        let quantity = Math.floor(Math.random()*(this.quantityMax-this.quantityMin+1)) + this.quantityMin;
        let cost = price * quantity;
        //get duration
        let duration = Math.floor(Math.random()*(this.durationMax-this.durationMin+1)) + this.durationMin;
        this.blackMarket[index] = new Deal(prodName, quantity, cost, duration);
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if hovering a GovDeal
     */
    hover(x,y) {
        if(this.govDealUnlocked) {
            const x1 = 1015;
            const y1 = 120;
            const w = 270;
            const h = 110;
            if(x >= x1 && x <= x1+w && y >= y1 && y <= y1+h) {
                this.tooltipDisplay = true;
                return true;
            }
            this.tooltipDisplay = false;
            return false;
        }
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if clic on one deal
     */
    clic(x,y) {
        const x1 = 1015;
        const y1 = 270;
        const w = 270;
        const h = 70;
        const distY = 90;
        if(x < x1 || x > x1+w) {
            return false;
        }
        for(let i=0; i<this.blackMarket.length; i++) {
            if(!this.blackMarket[i].deprecated && y >= y1+distY*i && y <= y1+distY*i+h) {
                this.secureDeal(this.blackMarket[i]);
                return true;
            }
        }
        if(this.hover(x,y)) {
            this.secureGovDeal(this.activeGovDeal());
            return true;
        }
        return false;
    },
    /**
     * @nocollapse
     * @returns {GovDeal} govdeal
     */
    activeGovDeal() {
        return this.governementMarket.filter(e => e.visible)[0];
    },
    /**
     * @nocollapse
     * @param {Deal} deal 
     */
    automaticBuy(deal) {
        if(deal.cost === deal.quantity) {
            this.secureDeal(deal);
        }
    },
    /**
     * @nocollapse
     */
    doubleEachGovDeal() {
        for(let index=0; index<this.governementMarket.length;index++) {
            this.governementMarket[index].remainingTime = this.governementMarket[index].startTime;
            this.governementMarket[index].cost = this.governementMarket[index].cost * 2;
            this.governementMarket[index].quantity = this.governementMarket[index].quantity * 2;            
        }
    },
    /**
     * @nocollapse
     * @param {Deal} deal 
     */
    secureDeal(deal) {
        if(ticketHandler.subtract(deal.cost)) {
            deal.remainingTime = -1;
            productHandler.addQuantityToProduct(deal.prodName, deal.quantity);
        }
    },
    /**
     * @nocollapse
     * @param {GovDeal} deal 
     */
    secureGovDeal(deal) {
        if(ticketHandler.subtract(deal.cost)) {
            productHandler.addQuantityToProduct(deal.prodName, deal.quantity);
            let index = -1;
            for(let i=0; i<this.governementMarket.length; i++) {
                if(this.governementMarket[i] === deal)
                    index = i+1;
            }
            if(index !== -1 && index < this.governementMarket.length) {
                this.governementMarket[index].visible = true;
            } else if(index !== -1) {
                index = 0;
                this.doubleEachGovDeal();
                this.governementMarket[index].visible = true;
            }
            productHandler.getProduct(deal.prodName).becomeVisible();
            UIHandler.newResources = true;
            deal.visible = false;
        } else if(deal.remainingTime <= 0) {
            gameHandler.state = "lost";
        }
    },

    /**
     * @nocollapse
     */
    core() {
        if(this.bmDealUnlocked) {
            if(this.blackMarket.length < 4) {
                for(let i=0; i<4; i++) {
                    this.addDeal(i);
                }
            }
            this.addDealMaybe();
            for(let i=0; i<this.blackMarket.length; i++) {
                if(this.blackMarket[i].advanceTime() && !this.blackMarket[i].deprecated) {
                    if(this.automaticDeals) {
                        this.automaticBuy(this.blackMarket[i]);
                    }
                    this.blackMarket[i].deprecated = true;
                }
            }
        }
        for(let i=0; i<this.governementMarket.length; i++) {
            if(this.governementMarket[i].visible) {
                if(this.governementMarket[i].advanceTime()) {
                    this.secureGovDeal(this.governementMarket[i]);
                }
            }
        }
    }
};

var ideaHandler = {
    ideaList: new Array(),
    tooltipDisplay: false,
    tooltipNumber: 0,
    research: false,
    researchOnNumber: 0,

    /**
     * @nocollapse
     */
    initialize() {
        this.research = false;
        this.researchOnNumber = 0;
        this.tooltipDisplay = false;
        this.tooltipNumber = 0;
        this.ideaList = new Array();
        //this.ideaList.push(new Idea(name, time, text1, text2, tooltip, unlock, effect));
        this.ideaList.push(new Idea("sellPermit", 250, "Declare to the government", "Your intentions to sell goods", "Don't forget to forge your grandmother signature on the A38 pass", ["buyPermit"], "maxCustomer+", true));
        this.ideaList.push(new Idea("buyPermit", 500, "Buy goods from the government", "", "It's the only legal way to restock shelves", ["blackMarket", "gulag", "maxCustomer1"], "govDeal"));

        //BlackMarket branch
        this.ideaList.push(new Idea("blackMarket", 1000, "Buy goods from the secret market", "Your neighbour taught you about", "Only open at night, say the word 'glasnot' to enter", ["priceMarket1"], "blackMarket"));
        this.ideaList.push(new Idea("priceMarket1", 3000, "Black Market is overpriced", "Negotiate better deals", "Saying the word 'apparatchik' help greatly for that", ["quantityMarket1"], "priceMarket-"));
        this.ideaList.push(new Idea("quantityMarket1", 4500, "Buy more on the black market", "Bulk buying is necessary", "They are not good at selling, you are", ["priceMarket2"], "quantityMarket+"));
        this.ideaList.push(new Idea("priceMarket2", 6000, "Black Market is overpriced", "Negotiate better deals", "Saying the word 'apparatchik' help greatly for that", ["quantityMarket2"], "priceMarket-"));
        this.ideaList.push(new Idea("quantityMarket2", 7500, "Buy more on the black market", "Bulk buying is necessary", "They are not good at selling, you are", ["priceMarket3"], "quantityMarket+"));
        this.ideaList.push(new Idea("priceMarket3", 9000, "Black Market is overpriced", "Negotiate better deals", "Saying the word 'apparatchik' help greatly for that", ["quantityMarket3"], "priceMarket-"));
        this.ideaList.push(new Idea("quantityMarket3", 10500, "Buy more on the black market", "Bulk buying is necessary", "They are not good at selling, you are", ["automaticDeals"], "quantityMarket+"));
        this.ideaList.push(new Idea("automaticDeals", 15000, "Pay someone to check prices", "And buy good deals for you", "All 1:1 deals are automatically bought", ["buyPassport"], "automaticDeals"));
        this.ideaList.push(new Idea("buyPassport", 21000, "Buy a totally official", "Passport", "You know someone that know someone that need 2000 tickets", ["goWest"], "buyPassport"));
        this.ideaList.push(new Idea("goWest", 30000, "Time to leave", "Cross the border", "What the exchange rate for tickets to euros?You'll need 10.000 tickets", [], "win"));

        //Work branch
        this.ideaList.push(new Idea("gulag", 1500, "Arrest the robbers, click to", "Send them to the Kommissar", "This is a crime-free country, not a gulag-free country", ["automaticGulag"], "clicGulag"));
        this.ideaList.push(new Idea("automaticGulag", 3000, "Take notes of who robs", "And automatically denounce them", "Sending some letters to the good people help the process", ["employRobbers"], "automaticGulag"));
        this.ideaList.push(new Idea("employRobbers", 4500, "Stop denouncing all robbers", "Some of them are employable", "Blackmail without using a stamp", ["wsSawMill"], "employRobbers"));
        this.ideaList.push(new Idea("wsSawMill", 6000, "SawMill", "Undust your old saw", "You could use the old crates to add value to the products", ["wsHarvestPotluck"], "wsSawMill"));
        this.ideaList.push(new Idea("wsHarvestPotluck", 9000, "Harvest Dandelions", "To make some soup and sell it", "Nobody cares what's in the soup, as soon as it's hot", ["wsTap"], "wsHarvestPotluck"));
        this.ideaList.push(new Idea("wsTap", 12000, "Cut the milk", "With tap water", "Less nutrients in each, but more redistribution", ["wsSewerageSkimming"], "wsTap"));
        this.ideaList.push(new Idea("wsSewerageSkimming", 15000, "Skim the sewerage", "To harvest oils", "Recycling is greener, and greener is redder", ["wsRatTrap"], "wsSewerageSkimming"));
        this.ideaList.push(new Idea("wsRatTrap", 18000, "Trap the rats", "Which swarm the stocks", "It's eat or be eaten, you've chose your team", ["wsCraftStation"], "wsRatTrap"));
        this.ideaList.push(new Idea("wsCraftStation", 21000, "Craft Station", "Make art", "Everybody wants a piece of our Great Leader", [], "wsCraftStation"));

        //Red Market branch
        this.ideaList.push(new Idea("maxCustomer1", 1500, "Pay a child to tell people", "About your great Market", "Probably more people will come here instead of the Green Market", ["maxCustomer2", "correctPrice"], "maxCustomerx"));
        this.ideaList.push(new Idea("maxCustomer2", 6000, "Pay a child to tell people", "About your great Market", "Probably more people will come here instead of the Green Market", ["maxCustomer3"], "maxCustomerx"));
        this.ideaList.push(new Idea("maxCustomer3", 12000, "Pay a child to tell people", "About your great Market", "Probably more people will come here instead of the Green Market", [], "maxCustomerx"));
        this.ideaList.push(new Idea("correctPrice", 12000, "Persuade customers to pay more", "Because you deserve it", "Inflation is hurting the people, and you are one of them", ["richerCustomer"], "correctPrice-"));
        this.ideaList.push(new Idea("richerCustomer", 15000, "Richer people come live nearby", "They can spend more", "Not even the government can stop gentrification", ["richerCustomer2"], "richCustomer+"));
        this.ideaList.push(new Idea("richerCustomer2", 21000, "Richer people come live nearby", "They can spend more", "Not even the government can stop gentrification", [], "richCustomer+"));
    },
    /**
     * @nocollapse
     */
    reload() {
        let newList = new Array();
        for(let i=0; i< this.ideaList.length; i++) {
            let oldIdea = this.ideaList[i];
            let newIdea = new Idea(oldIdea.name, oldIdea.startTime, oldIdea.text1, oldIdea.text2, "test", JSON.parse(JSON.stringify(oldIdea.unlock)), oldIdea.effect, oldIdea.visible);
            newIdea.remainingTime = oldIdea.remainingTime;
            newIdea.tooltip = JSON.parse(JSON.stringify(oldIdea.tooltip));
            newList.push(newIdea);
        }
        this.ideaList = newList;
    },
    /**
     * @nocollapse
     */
    draw() {
        ctxInfos.clearRect(1005,70,290,565);
        ctxInfos.fillStyle = "black";
        ctxInfos.font = "30px kenney_miniregular";
        ctxInfos.fillText("Ideas:", 1020, 105, 260);
    },
    /**
     * @nocollapse
     */
    drawIdeas() {
        let list = this.getVisibleIdeas();
        const x = 1015;
        const y = 120;
        const distY = 120;
        for(let i=0; i<list.length; i++) {
            list[i].draw(x, y+distY*i);
        }
        if(this.tooltipDisplay) {
            this.drawTooltip();
        }
    },
    /**
     * @nocollapse
     */
    drawTooltip() {
        const h = 110;
        const w = 270;
        const x = 1015;
        const y = 120;
        const distY = 120;
        let list = this.getVisibleIdeas();
        list[this.tooltipNumber].drawTooltip(x,distY*this.tooltipNumber+y+h/2);
    },
    /**
     * @nocollapse
     * @returns {Array} array of ideas
     */
    getVisibleIdeas() {
        return this.ideaList.filter(e => e.visible);
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y
     * @returns {boolean} true if hovering 
     */
    hover(x,y) {
        const h = 110;
        const w = 270;
        const x1 = 1015;
        const y1 = 120;
        const distY = 120;
        if(x >= x1 && x <= x1 + w) {
            let list = this.getVisibleIdeas();
            for(let i=0; i<list.length; i++) {
                if(y >= y1+distY*i && y <= y1+h+distY*i) {
                    this.tooltipDisplay = true;
                    this.tooltipNumber = i;
                    return true;
                }
            }
        }
        this.tooltipDisplay = false;
        return false;
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if clic on research
     */
    clic(x,y) {
        const h = 110;
        const w = 270;
        const x1 = 1015;
        const y1 = 120;
        const distY = 120;
        if(x >= x1 && x <= x1 + w) {
            let list = this.getVisibleIdeas();
            for(let i=0; i<list.length; i++) {
                if(y >= y1+distY*i && y <= y1+h+distY*i) {
                    if(this.research && this.researchOnNumber == i) {
                        this.research = false;
                        list[i].searching = false;
                    } else {
                        this.research = true;
                        if(list[this.researchOnNumber] !== undefined) {
                            list[this.researchOnNumber].searching = false;
                        }
                        this.researchOnNumber = i;
                        list[i].searching = true;
                    }
                    return true;
                }
            }
        }
        return false;
    },
    /**
     * @nocollapse
     * @param {string} ideaName
     * @returns {Idea | undefined} Idea
     */
    getIdea(ideaName) {
        let idea = this.ideaList.filter(e => e.name === ideaName);
        if(idea.length === 0)
            return undefined;
        return idea[0];
    },
    /**
     * @nocollapse
     * @param {Idea} idea 
     */
    completeIdea(idea) {
        let blocked = false;
        switch(idea.effect) {
            case "maxCustomer+":
                customerHandler.maxCustomerInMarket = 5;
                break;
            case "govDeal":
                dealHandler.govDealUnlocked = true;
                UIHandler.unlockTab("deals");
                break;
            case "blackMarket":
                dealHandler.bmDealUnlocked = true;
                if(UIHandler.menuOpen === "deals") {
                    dealHandler.draw();
                }
                break;
            case "automaticDeals":
                dealHandler.automaticDeals = true;
                break;
            case "maxCustomerx":
                customerHandler.maxCustomerInMarket *= 2;
                break;
            case "correctPrice-":
                productHandler.increaseCorrectPrice();
                break;
            case "clicGulag":
                customerHandler.clicGulag = true;
                break;
            case "automaticGulag":
                customerHandler.automaticGulag = true;
                break;
            case "employRobbers":
                employmentHandler.employmentUnlocked = true;
                UIHandler.unlockTab("employ");
                break;
            case "richCustomer+":
                customerHandler.maxTickets *= 2;
                break;
            case "priceMarket-":
                dealHandler.deltaToCorrectPriceMax--;
                dealHandler.deltaToCorrectPriceMin--;
                break;
            case "quantityMarket+":
                dealHandler.quantityMin *= 2;
                dealHandler.quantityMax = Math.round(dealHandler.quantityMax*1.5);
                break;
            case "buyPassport":
                if(!ticketHandler.subtract(2000)) {
                    blocked = true;
                    idea.remainingTime = 10;
                }
                break;
            case "win":
                if(!ticketHandler.subtract(10000)) {
                    blocked = true;
                    idea.remainingTime = 10;
                } else {
                    gameHandler.state = "win";
                }
                break;
            case "wsSawMill":
                employmentHandler.unlockStation("Sawmill");
                break;
            case "wsTap":
                employmentHandler.unlockStation("Tap");
                break;
            case "wsRatTrap":
                employmentHandler.unlockStation("Rat Trap");
                break;
            case "wsSewerageSkimming":
                employmentHandler.unlockStation("Sewerage Skimming");
                break;
            case "wsCraftStation":
                employmentHandler.unlockStation("Craft Station");
                break;
            case "wsHarvestPotluck":
                employmentHandler.unlockStation("Harvest Potluck");
                break;                
        }
        if(!blocked) {
            this.research = false;
            idea.visible = false;
            this.tooltipDisplay = false;
            for(let i=0; i<idea.unlock.length; i++) {
                let idea2 = this.getIdea(idea.unlock[i]);
                if(idea2 !== undefined)
                    idea2.visible = true;
            }
        }
    },

    /**
     * @nocollapse
     */
    core() {
        if(this.research) {
            let idea = this.getVisibleIdeas()[this.researchOnNumber];
            if(idea.advanceTime()) {
                this.completeIdea(idea);
            }
        }
    }
};

var employmentHandler = {
    employmentUnlocked: false,
    workStation: new Array(),
    tooltipDisplay: false,
    tooltipNumber: 0,
    ungulaged: 0,
    countToEmploy: 9,

    /**
     * @nocollapse
     */
    initialize() {
        this.countToEmploy = 9;
        this.tooltipNumber = 0;
        this.ungulaged = 0;
        this.tooltipDisplay = false;
        this.workStation = new Array();
        this.employmentUnlocked = false;
        //this.workStation.push(new WorkStation(name, productOrigin, productGoal, tooltip, timeToComplete, quantityCreated));
        this.workStation.push(new WorkStation("Sawmill", "Wheat Flour", "Buckwheat Flour", "Cut all wooden crates, add the dust to the wheat, and voila!", 1500, 3));
        this.workStation.push(new WorkStation("Harvest Potluck", "Seasonal Vegetables", "Potluck Soup", "There's dandelions everywhere, and in this soup too", 3000, 4));
        this.workStation.push(new WorkStation("Tap", "Cow Milk", "Skim Milk", "Just a bit of water in each", 3000, 2));
        this.workStation.push(new WorkStation("Sewerage Skimming", "Rapeseed Oil", "Frying Oil", "This is gold sitting beneath your feet, might aswell use it", 2500, 6));
        this.workStation.push(new WorkStation("Rat Trap", "Beef Meat", "Ground Beef", "Help the city by chasing rats, help the customers, win win win", 2000, 4));
        this.workStation.push(new WorkStation("Craft Station", "Great Leader Portrait", "Collectible Thumbnails", "Heresy for the state, fun for the children", 6000, 50));
    },
    /**
     * @nocollapse
     */
    reload() {
        let newList = new Array();
        for(let i=0; i<this.workStation.length; i++) {
            let oldWS = this.workStation[i];
            let newWS = new WorkStation(oldWS.name, oldWS.productOrigin, oldWS.productGoal, "", oldWS.timeToComplete, oldWS.quantityCreated);
            newWS.tooltip = JSON.parse(JSON.stringify(oldWS.tooltip));
            newWS.remainingTime = oldWS.remainingTime;
            newWS.visible = oldWS.visible;
            newWS.numWorkers = oldWS.numWorkers;
            newList.push(newWS);
        }
        this.workStation = newList;
    },
    /**
     * @nocollapse
     */
    draw() {
        ctxInfos.clearRect(1005,70,290,565);
        ctxInfos.fillStyle = "black";
        ctxInfos.font = "25px kenney_miniregular";
        ctxInfos.fillText("Ungulaged:", 1020, 105, 260);
        ctxInfos.fillText("Restockers:", 1020, 130, 260);
        //clics
        ctxInfos.drawImage(img.down, 1240, 115, 16, 16);
        ctxInfos.drawImage(img.up, 1260, 115, 16, 16);
    },
    /**
     * @nocollapse
     */
    drawStation() {
        ctxFront.fillStyle = "black";
        ctxFront.font = "25px kenney_miniregular";
        //draw number of people employed to stock
        let numStockers = employeeHandler.employees.length;
        ctxFront.fillText(numStockers, 1180, 130, 50);
        //number of people ready to employ
        ctxFront.fillText(this.ungulaged, 1170, 105, 260);

        //draw each stations
        const x = 1015;
        const y = 150;
        const distY = 80;
        let stationList = this.listWorkStation();
        for(let i=0; i<stationList.length; i++) {
            stationList[i].draw(x, y+distY*i);
        }
        if(this.tooltipDisplay)
            this.drawTooltip();
    },
    /**
     * @nocollapse
     */
    drawTooltip() {
        const h = 70;
        const w = 270;
        const x = 1015;
        const y = 150;
        const distY = 80;
        let list = this.listWorkStation();
        list[this.tooltipNumber].drawTooltip(x,distY*this.tooltipNumber+y+h/2);
    },
    /**
     * @nocollapse
     */
    gulag() {
        this.countToEmploy++;
        if(this.countToEmploy >= 5) {
            this.countToEmploy = 0;
            this.ungulaged++;
        }
    },
    /**
     * @nocollapse
     * @returns {Array} array of work stations visible
     */
    listWorkStation() {
        return this.workStation.filter(e => e.visible);
    },
    /**
     * @nocollapse
     * @returns {Array} array of workstation
     */
    listWorkStationManned() {
        return this.workStation.filter(e => e.visible && e.numWorkers>0);
    },
    /**
     * @nocollapse
     * @param {string} station name
     */
    unlockStation(station) {
        let ws = this.workStation.filter(e => e.name === station);
        console.assert(ws.length > 0, station);
        if(ws.length > 0) {
            ws[0].visible = true;
            let prod = productHandler.getProduct(ws[0].productGoal);
            prod.becomeVisible();
            UIHandler.newResources = true;
        }
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if clic
     */
    clic(x,y) {
        if(y >= 115 && y <= 131) {
            if(x >= 1240 && x <= 1256) {
                if(employeeHandler.employees.length > 0) {
                    employeeHandler.unSpawnEmployee();
                    this.ungulaged++;
                }
                return true;
            }
            if(x >= 1260 && x <= 1276) {
                if(this.ungulaged > 0) {
                    this.ungulaged--;
                    employeeHandler.spawnEmployee();
                }
                return true;
            }
        }
        const w = 16;
        const h = 16;
        const x1 = 1215;
        const y1 = 184;
        const distY = 80;
        const distX = 20;
        let list = this.listWorkStation();
        for(let i=0; i<list.length; i++) {
            let ws = list[i];
            if(x >= x1 && x <= x1 + w && y >= y1+i*distY && y <= y1+i*distY+h) {
                if(ws.numWorkers > 0) {
                    ws.numWorkers--;
                    this.ungulaged++;
                }
                return true;
            }
            if(x >= x1+distX && x <= x1+distX+w && y >= y1+i*distY && y <= y1+i*distY+h) {
                if(this.ungulaged > 0) {
                    ws.numWorkers++;
                    this.ungulaged--;
                }
                return true;
            }
        }
        return false
    },
    /**
     * @nocollapse
     * @param {number} x 
     * @param {number} y 
     * @returns {boolean} true if hover
     */
    hover(x,y) {
        //TODO
        const h = 70;
        const w = 270;
        const x1 = 1015;
        const y1 = 150;
        const distY = 80;
        if(x >= x1 && x <= x1 + w) {
            let list = this.listWorkStation();
            for(let i=0; i<list.length; i++) {
                if(y >= y1+distY*i && y <= y1+h+distY*i) {
                    this.tooltipDisplay = true;
                    this.tooltipNumber = i;
                    return true;
                }
            }
        }
        this.tooltipDisplay = false;
        return false;
    },
    /**
     * @nocollapse
     * @param {string} productName 
     * @param {number} quantity
     */
    addProduct(productName, quantity) {
        productHandler.addQuantityToProduct(productName, quantity);
    },
    /**
     * @nocollapse
     * @param {WorkStation} workStation 
     * @returns {boolean} true if possible to start
     */
    kickStart(workStation) {
        let prodO = productHandler.getProduct(workStation.productOrigin);
        if(prodO !== undefined && prodO.getQuantity() > 0) {
            prodO.subtract();
            return true;
        }
        return false;
    },

    /**
     * @nocollapse
     */
    core() {
        let list = this.listWorkStationManned();
        for(let i=0; i<list.length; i++) {
            let ws = list[i];
            let dontAdvance = false;
            if(ws.remainingTime === ws.timeToComplete) {
                dontAdvance = true;
                if(this.kickStart(ws)) {
                    dontAdvance = false;
                }
            }
            if(!dontAdvance) {
                if(ws.advanceTime()) {
                    productHandler.addQuantityToProduct(ws.productGoal, ws.quantityCreated);
                }
            }
        }

    }
};

/*
------------------------------
ALGOS
------------------------------
*/

/**
 * Help js sort() with shelves
 * @param {Shelf} a 
 * @param {Shelf} b 
 * @returns {number} -1, 0 or 1
 */
function compareShelvesByQuantityAddable(a, b) {
    if(a.getQuantityAddable() < b.getQuantityAddable())
        return 1;
    if(a.getQuantityAddable() > b.getQuantityAddable())
        return -1;
    return 0;
}

/**
 * use preferredDirection when delta x is sufficiently big
 * @param {Object} start {x, y} start
 * @param {Object} goal {x, y} goal
 * @param {number} speed
 * @param {string} preferredDirection
 * @param {Object} size {w, h}
 * @returns {string} direction ["top", "left" ...]
 */
function path(start, goal, speed, preferredDirection, size) {
    //console.log(start, goal, speed, preferredDirection, size)
    let dir = "top";
    let newPos = {x: start.x, y: start.y};
    let deltaX = goal.x - start.x;
    let deltaY = goal.y - start.y;
    let deltaAbs = Math.abs(deltaX);
    let notFound = true;

    //if deltaX big enough, go left or right
    if(deltaAbs > speed) {
        newPos.x += Math.round(deltaX/deltaAbs)*speed;
        //console.log(start, newPos, size);
        if(!marketHandler.doICollide(newPos.x, newPos.y, size.w, size.h))
            notFound = false; 
    }
    
    //if not possible, and deltaX big enough, go preferred direction
    if(notFound && deltaAbs > speed) {
        newPos.x = start.x;
        if(preferredDirection === "top") {
            newPos.y -= speed;
        } else {
            newPos.y += speed;
        }
        if(!marketHandler.doICollide(newPos.x, newPos.y, size.w, size.h))
            notFound = false;
    }

    //if not possible, and deltaX little, go top or bottom
    if(notFound) {
        newPos.x = start.x;
        newPos.y = start.y;
        if(deltaY < 0) {
            newPos.y -= speed;
        } else {
            newPos.y += speed;
        }
        //console.log(start, newPos,marketHandler.doICollide(newPos.x, newPos.y, size.w, size.h));
        if(!marketHandler.doICollide(newPos.x, newPos.y, size.w, size.h))
            notFound = false;
    }

    //console.assert(!notFound, start, goal);

    //calculate dir of newPos
    deltaX = newPos.x - start.x;
    deltaY = newPos.y - start.y;
    if(deltaX < 0) {
        dir = "left";
    } else if(deltaX > 0) {
        dir = "right";
    } else if(deltaY < 0) {
        dir = "top";
    } else {
        dir = "bottom";
    }
    return dir;
}

/**
 * main loop, launch gameHandler.core() each time
 * @param {number} timestamp 
 */
function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        if(!gameHandler.amIWorking) {
            deltaTimeElapsed = timestamp - lastTime;
            timer += deltaTimeElapsed;
            lastTime = timestamp;
            //console.time("core");
            gameHandler.core();
            //console.timeEnd("core");
        }
    }
    window.requestAnimationFrame(animate);
}

/**
 * @param {number} ticks
 * @returns {string} ticks in h/m/s 
 */
function ticksToText(ticks) {
    let tickCount = Math.round(ticks*refresh/1000);
    let second, min, hour;
    second = tickCount%60;
    tickCount -= second;
    if(tickCount > 0) {
        tickCount = Math.round(tickCount/60);
        min = tickCount%60;
        tickCount -= min;
        if(tickCount > 0) {
            tickCount = Math.round(tickCount/60);
            hour = tickCount;
            return hour + "h " + min + "min " + second + "s";
        } else {
            return min + "min " + second + "s";
        }
    } else {
        return second + "s";
    }
    return "";
}

/*
------------------------------
USER INTERACTIONS
------------------------------
*/

document.addEventListener('mousemove', e => {
    canvasPosition = canvasFront.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
    UIHandler.hover(mouse.x,mouse.y);
});

document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});

function mouseDown() {
    UIHandler.clic();
}

function mouseUp() {

}

/*
------------------------------
PAGE START
------------------------------
*/

window.onload = function() {
    gameHandler.initialize();
}